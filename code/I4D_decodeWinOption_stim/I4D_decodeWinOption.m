
function I4D_decodeWinOption(rootpath, ID)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    % train across dims, test on each Dim separately (and on uncued)
    
    % TO DO:
    % adjust data input
    % implement N fold cross-validation
    
    %% add paths

    if ismac
        ID = '1117';
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..','..'))
        rootpath = pwd;
    end
    
    pn.libsvm   = fullfile(rootpath, 'tools', 'libsvm'); addpath(genpath(pn.libsvm));
    pn.data     = fullfile(rootpath, 'data');
%     pn.trlinfo  = fullfile(rootpath, 'data', 'C_trialInfo');
	pn.out      = fullfile(rootpath, 'data', 'B_DecodingResults');
    pn.EEG      = fullfile(rootpath, '..', 'X1_preprocEEGData');

    mex -setup
    cd(fullfile(pn.libsvm,'matlab'));
    make

    % load EEG data
    load(fullfile(pn.EEG,[ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']))
    
    % downsample broadband time series
    stepsize = 0.05; % in s
    samples = floor(stepsize/(1/data.fsample)/2);
    timeWindows = -.5:stepsize:7.5;    
    for indTrial = 1:numel(data.trial)
        [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
        for indTP = 1:numel(tmpPoint)
            data.trialavg{indTrial}(:,indTP) = ...
                nanmean(data.trial{indTrial}(:,tmpPoint(indTP)-samples:tmpPoint(indTP)+samples),2);
        end
    end

    data.trial = data.trialavg;
    
    load(fullfile(pn.data, 'A_winOptions.mat'), 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix
    
    N_chans = size(data.trial{1},1);
    N_time = size(data.trial{1},2);
    N_conditions = 2;
    Ntrials_available = numel(data.trial);
    
    numIncluded = [];
    for indDim = 1:4
        for indAtt = 1:4
            disp(num2str(indAtt));
            D{indDim,indAtt} = NaN(2,8,N_chans,N_time); % condition*trial*channel*time matrix
            curTrial = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == indDim); % attribute is probed and cue dim is x
            curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(curTrial,7)));
            for indOption = 1:2
                includedTrials = curTrial(curTrialWinMiss==indOption);
                numIncluded(indDim,indAtt, indOption) = numel(includedTrials);
                for indTrial = 1:numIncluded(indDim,indAtt, indOption)
                    D{indDim,indAtt}(indOption,indTrial,:,1:N_time) = data.trial{includedTrials(indTrial)}(:,:);
                    % keep track of which trials we analyze (we need to always exlude the tested trial, otherwise we bias the solution)
                    IncludedTrials{indDim,indAtt,indOption} = includedTrials;
                end
            end
        end
    end
    
    %% 3) Decoding
    
    % How to create a single-trial test set here?
    % For each attribute, create a decoder trained on exemplars from all Dims
    % This decoder targets the contrast between attributes
    % Apply this trained decoder on each single trial
    % The trial in question must be excluded from the training set
    % Perform 100 permutations of training trials to bootstrap single-trial
    % testing accuracy
    
    num_permutations=100;
    totalperms = Ntrials_available*4*num_permutations; count = 0;
    DA=NaN(Ntrials_available,4,num_permutations,N_conditions,N_conditions,N_time);
    for indTrial = 1:Ntrials_available
        for indAtt = 1:4
            for perm =1:num_permutations
                count = count+1;
                tic

                % reserve 1 random trial for each condition that will not be
                % included in training set
                
                train = cell(2,4); % training set specific to dim 1, attribute
                for indDim_s = 1:4
                    for indAtt_s = 1:4
                        for indCond = 1:2
                            trialsAvailable     = find(~isnan(D{indDim_s,indAtt_s}(indCond,:,1,1)));
                            % check if the current test trial is in the
                            % set, if so remove it!
                            curTestPosinSet = find(ismember(IncludedTrials{indDim_s,indAtt_s,indCond}(trialsAvailable),indTrial));
                            if ~isempty(curTestPosinSet)
                                trialsAvailable(curTestPosinSet) = [];
                            end
                            randomTrials        = trialsAvailable(randperm(numel(trialsAvailable)));
                            % create three folds
                            Nfold = 3;
                            Nperfold = floor(numel(trialsAvailable)/Nfold);
                            trialsTrain{1} = randomTrials(1:Nperfold);
                            trialsTrain{2} = randomTrials(Nperfold+1:2*Nperfold);
                            trialsTest = randomTrials(2*Nperfold+1:end);
                            if indDim_s < 5
                                train{indCond,indAtt_s} = cat(2, train{indCond,indAtt_s}, ...
                                    nanmean(D{indDim_s,indAtt_s}(indCond,trialsTrain{1},:,:),2),...
                                    nanmean(D{indDim_s,indAtt_s}(indCond,trialsTrain{2},:,:),2));
                            end
                        end
                    end
                end
                
                % use max. available number of training trials
                
                for indAtt_s = 1:4
                    for indCond = 1:2
                        L(indCond,indAtt_s) = size(train{indCond,indAtt_s},2);
                    end
                end

                %% perform SVM classification

                [~, traintime] = min(abs(timeWindows-3.2));

                for condA=1:N_conditions %loop through all conditions
                    for condB = condA+1:N_conditions  %loop through all conditions >condA+1
                        if any(isnan(train{condA,indAtt}))
                            warning('trials missing')
                            continue;
                        end
                        for time_point =1:N_time % all time points are independent
                            % L-1 pseudo trials go to testing set, the Lth to training set
                            MEEG_training_data=[squeeze(train{condA,indAtt}(1,1:L(condA,indAtt),:,traintime)); ...
                                squeeze(train{condB,indAtt}(1,1:L(condB,indAtt),:,traintime))];
                            % choose current testing trial
                            MEEG_testing_data=[squeeze(data.trial{indTrial}(:,time_point))];
                            % labels for training
                            labels_train=[ones(1,L(condA,indAtt)) 2*ones(1,L(condB,indAtt))]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                            % encode label of the current test trial
                            labels_test=squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(indTrial,7)));
                            % train SVM
                            model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                            % test SVM
                            [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data' , model);
                            % accuracy will be either 0 or 100%; this
                            % should average to a more reasonable estimate
                            % due to bootstrapping
                            if ~isempty(accuracy)
                                DA(indTrial,indAtt,perm,condA,condB,time_point)=accuracy(1);
                            else
                                DA(indTrial,indAtt,perm,condA,condB,time_point)=NaN;
                            end
                        end % time point
                    end % cond A
                end %cond B
                ttoc = toc;
                disp(['Perm ', num2str(count), '/', num2str(totalperms), ': ', num2str(ttoc), 's'])
            end % permutation
        end % attribute loop
    end
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,3));
    DA_std = squeeze(nanstd(DA,[],3));
    
   %figure; plot(squeeze(DA_end(1,1,1,2,:)))
   %figure; plot(squeeze(nanmean(nanmean(DA_end(:,1:4,1,2,:),2),1)))
  
    %% save results

    save(fullfile(pn.out, [ID, '_I4D_decodeWinOption.mat']), 'DA_end', 'DA_std');
    
end % function end