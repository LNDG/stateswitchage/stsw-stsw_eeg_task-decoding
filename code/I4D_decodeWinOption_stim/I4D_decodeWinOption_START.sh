#!/bin/bash

fun_name="I4D_decodeWinOption"
job_name="stsw_decode_eeg_3_2"

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

mkdir ${rootpath}/log

# path to the text file with all subject ids:
#path_ids="${rootpath}/code/id_list_mr.txt"
# read subject ids from the list of the text file
#IDS=$(cat ${path_ids} | tr '\n' ' ')

IDS="1117 1118 1120 1124 1126 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2132 2133 2134 2135 2139 2140 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2238 2241 2244 2246 2248 2250 2251 2252 2258 2261"

for subj in ${IDS}; do
	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 1 \
  		--mem 4gb \
  		--time 04:00:00 \
  		--output ${rootpath}/log/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2016b; matlab -nodisplay -r \"${fun_name}('${rootpath}','${subj}')\""
done