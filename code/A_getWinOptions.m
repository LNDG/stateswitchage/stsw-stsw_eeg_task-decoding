%% assign winning option to each trial (50% binary split left vs. right)

    % load merged behavioral data
    behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');

    % get trials where two options (in two-state scenario) align
    % operationalized via the relative response agreement among the cues

    winningOptions = NaN(4, 102, 256);

    for indID = 1:numel(behavData.MergedDataEEG.expInfo) % loop across subjects
        count = 1;
        for indRun = 1:4 % loop across runs
            for indBlock = 1:8 % loop across blocks
                for indTrial = 1:8 % loop across trials
                    if ~isempty(behavData.MergedDataEEG.expInfo{indID})
                        cuedAttributes = behavData.MergedDataEEG.expInfo{indID}.AttCuesRun{indRun}{indBlock,indTrial};
                        for indFeature = 1:4
                            winningOptions(indFeature,indID,count) = behavData.MergedDataEEG.expInfo{indID}.HighProbChoiceRun{indRun}{indBlock,indTrial}(indFeature);
                        end
                        count = count + 1; % increase trial number;
                    else
                       winningOptions(:,indID,:) = NaN;
                    end
                end
            end
        end
    end
    
    winOptions.winningOptions = winningOptions;
    winOptions.IDs = behavData.IDs_all;
    
    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/A_winOptions.mat'], 'winOptions')
    