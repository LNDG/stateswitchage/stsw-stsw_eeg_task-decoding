
function I_decodeTarget_D1(indID)

    % inputs:   ID   | indicator for ID list below
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
        
    % downsample broadband time series
    timeWindows = -.5:0.01:7.5;    
    for indTrial = 1:numel(data.trial)
        [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
        data.trial{indTrial} = data.trial{indTrial}(:,tmpPoint);
    end
    
    for indDim = 1:4
        C{indDim} = NaN(4,16,60,numel(timeWindows)); % condition*trial*channel*time matrix
        for indAtt = 1:4
            disp(num2str(indAtt));    
            includedTrials = find(data.TrlInfo(:,8) == indDim & data.TrlInfo(:,6) == indAtt);
            numIncluded(indDim, indAtt) = numel(includedTrials);
            for indTrial = 1:numIncluded(indDim, indAtt)
                trialSize = size(data.trial{includedTrials(indTrial)},2);
                C{indDim}(indAtt,indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    %% univariate noise normalization

    %baseline_time_points=1:1000; % pre-cue interval baseline

    M=size(C{1},1);
    N=size(C{1},2);
    O=size(C{1},3);
    T=size(C{1},4);

    for indDim = 1:4
%         baseline_mean=nanmean(C{indDim}(:,:,:,baseline_time_points),4);
%         baseline_std=nanstd(C{indDim}(:,:,:,baseline_time_points),[],4);
%         D{indDim}=(C{indDim}-baseline_mean)./baseline_std;
        D{indDim}=C{indDim};
    end; clear C;

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we basically run 4 separate decodings. Note that we
    % always use the Dim1 condition to create the training set however.
    
    num_permutations=100;
    DA=NaN(4,num_permutations,M,M,T);
    Topo=NaN(4,num_permutations,M,M,T,O);
    for indDim = 1:4
        for perm =1:num_permutations
            tic
            
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min(min(numIncluded));
            for indD = unique([1, indDim])
                permutedD{indD} = NaN(4,subsetAmount,60,T);
                for indCond = 1:4
                    trialsAvailable          = find(~isnan(D{indD}(indCond,:,1,1)));
                    trialsSelected           = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                    permutedD{indD}(indCond,:,:,:)      = D{indD}(indCond,trialsSelected,:,:);
                end
            end

            %% bin data into L=N/K pseudo trials
            % Not done here due to low N.
            
%             K=1;
%             L=subsetAmount/K;
%             for indD = unique([1, indDim])
%                 pseudo_trialD{indD}=NaN(M,L,O,T);
%                 for step=1:L %average by steps
%                     trial_selector=(1+(step-1)*K):(K+(step-1)*K); %select trials to be averaged
%                      pseudo_trialD{indD}(:,step,:,:)= mean(permutedD{indD}(:,trial_selector,:,:),2); %assign pseudo trial to pseudo_trial_D
%                 end
%             end

            L = subsetAmount;

            %% perform SVM classification
            
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(permutedD{1}(condA,1:end-1,:,time_point)) ; squeeze(permutedD{1}(condB,1:end-1,:,time_point))];
                        MEEG_testing_data=[squeeze(permutedD{indDim}(condA,end,:,time_point))' ; squeeze(permutedD{indDim}(condB,end,:,time_point))'];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,L-1) 2*ones(1,L-1)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test= [1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indDim,perm,condA,condB,time_point)=accuracy(1);
                        % get topography of decision boundary
                        Topo(indDim,perm,condA,condB,time_point,:) = model.SVs' * model.sv_coef; % b = -model.rho;
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
        
%         SV = model.SVs' * model.sv_coef;
%         DV = SV'*MEEG_testing_data(1,:)'-model.rho;

    end % target dimensionality loop
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    Topo = squeeze(nanmean(Topo,2));
    
%     figure; imagesc(timeWindows,[],squeeze(nanmean(nanmean(Topo(1,:,:,:,:),2),3)))
%     figure; imagesc(timeWindows,[],squeeze(DA_end(:,1,2,:)))

    %% save results

    save([pn.out, ID, '_DecodeTargetMultiState_v3.mat'], 'DA_end', 'DA_std', 'Topo');
    
end % function end