% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

DA_merged = cell(1,2); 
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeWinOption_SSVEP_Peak.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            %DA_merged{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        %if numel(DA_end)>2
            DA_merged{indAge}(id,:,:,:,:) = DA_end{1};
        %end
    end
end

DA_merged{indAge}(DA_merged{indAge}==0) = NaN;

%% plot results

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

h = figure;
    curData = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1,:,:,:),4),3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1} = shadedErrorBar([],squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
     curData = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,2,:,:,:),4),3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1} = shadedErrorBar([],squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);

     curData = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,3,:,:,:),4),3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1} = shadedErrorBar([],squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
 curData = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,4,:,:,:),4),3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{1} = shadedErrorBar([],squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);


%ylim([50, 57])
title("Decoding winning option (Dim1)")
xlabel("Time (s from stimulus onset)");
ylabel("Decoding accuracy (%)")
