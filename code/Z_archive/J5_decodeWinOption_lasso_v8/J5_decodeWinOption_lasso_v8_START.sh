#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/J5_decodeWinOption_lasso_v8

subjectAmount=100
for i in $(seq 1 $subjectAmount); do
	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name J5_lasso_${i}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 1"				>> job.slurm
	echo "#SBATCH --mem 6gb" 						>> job.slurm
	echo "#SBATCH --time 02:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/J5_lasso_${i}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./J5_decodeWinOption_lasso_v8_run.sh /opt/matlab/R2016b $i" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done