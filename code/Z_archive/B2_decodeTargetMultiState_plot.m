% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeTargetMultiState_v2.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_end;%zscore(DA_end,[],4);
    end
end

%% plot results

time = -1.5:.008:1400*.008+-1.5;
time(end) = [];

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
    
h = figure;
patches.timeVec = [-5 0 1 3 6 8 10];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
hold on;
for indCond = 1:4
    condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:),3),4),2));
    curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,indCond,:,:,:),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indCond} = shadedErrorBar(time,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 1}, 'patchSaturation', .25);
end
ylim([48 54])
title("Decoding prevalent option")
xlabel("Time (s) from cue presentation"); ylabel('Decoding accuracy (%)')
xlim([-1.5 8])

%% average across all features

h = figure;
patches.timeVec = [-5 0 1 3 6 8 10];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [0 0 [100 100]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
hold on;
    condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:),3),4),2));
    standError = nanstd(condAvg,[],1)./sqrt(size(condAvg,1));
    dimPlot{indCond} = shadedErrorBar(time,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 1}, 'patchSaturation', .25);
ylim([48.5 52])
title({"Decoding prevalent option";"(avg. across all features; incl. all trials)"})
xlabel("Time (s) from cue presentation"); ylabel('Decoding accuracy (%)')
xlim([-1.5 8])

set(findall(gcf,'-property','FontSize'),'FontSize',18)
