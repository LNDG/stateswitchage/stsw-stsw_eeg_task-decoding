% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

% load behavioral data

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_Decode16Comb_SSVEP.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{id}(indCount,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_end;
    end
end

%% plot results

% figure; imagesc(squeeze(nanmean(nanmean(DA_merged{1},2),3)))
% figure; imagesc(squeeze(nanmean(nanmean(DA_merged{1},4),1)))

%% plot results

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

time = 1:.1:8-((30.*(1/30))./2);

h = figure;
    patches.timeVec = [0 3.04];
    patches.colorVec = [.9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,:,:,:),3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    shadedErrorBar(time-3,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);

ylim([48, 52])
title('Decoding combinatorials (all trials; normalized SSVEP)')
xlabel('Time (s from stimulus onset)');
ylabel('Decoding accuracy (%)')

%% explore decoding differences between combinatorials

curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,:,:,time>3 & time<6),4),1));

figure; imagesc(curData)

%% plot dynamics for every contrast

curData = reshape(squeeze(DA_merged{indAge}(:,:,:,:)), size(DA_merged{indAge},1), 16*16, size(DA_merged{indAge},4));
curData = squeeze(nanmean(curData,1));
figure; plot(curData')