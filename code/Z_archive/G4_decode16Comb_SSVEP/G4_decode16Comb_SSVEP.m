% 170926 | JQK adapted script from MRC (Cichy)

function G_decode16Comb(indID)

    % inputs:   ID   | indicator for ID list below

    % For each trial, each feature is defined by one of two options
    % Try to decode the objectively winning option on a single trial basis
    % For each target level, train and test for each feature across all trials
    % This tests: within target level, is it possible to decode winning feature?
    % In load 1 this should only be possible if it is contained in feature is contained in the attentional set
    % In load 4 this should always be possible
    
    % Follow up: decode for in- vs. out-of-set by target load (but: maximum 16 trials available)
    % alternative: train on L1 in-set, test on all other conditions
    
    %% add paths

    if ismac
        pn.root     = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
        addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/study_information/paradigm/MAT/functions/allcomb/');
        pn.FieldTrip    = [pn.root, 'T_tools/fieldtrip/']; addpath(pn.FieldTrip); ft_defaults;

    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
%      %% CSD transform
%     
%     TrlInfo = data.TrlInfo;
%     TrlInfoLabels = data.TrlInfoLabels;
%     
%     csd_cfg = [];
%     csd_cfg.elecfile = 'standard_1005.elc';
%     csd_cfg.method = 'spline';
%     data = ft_scalpcurrentdensity(csd_cfg, data);
%     data.TrlInfo = TrlInfo; clear TrlInfo;
%     data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
    
    %% calculate FFT for stim period from ERP
    
    ncyc = 30;
    timeWindow = ncyc.*(1/30); % 1s sampling window
    stepSize = .1; % 100 ms step size
    timeWindows = 1:stepSize:8-(timeWindow./2);
    
    MTMdata = [];
    for indWindow = 1:numel(timeWindows)
        cfg = [];
        cfg.toilim = [timeWindows(indWindow) timeWindows(indWindow)+timeWindow];
        curData = ft_redefinetrial(cfg, data);
        curData = rmfield(curData, 'TrlInfo');
        curData = rmfield(curData, 'TrlInfoLabels');
        cfg              = [];
        cfg.output       = 'pow';
        cfg.channel      = 'all';
        cfg.method       = 'mtmfft';
        cfg.taper        = 'hanning';
        cfg.keeptrials   = 'yes';
        cfg.pad          = ceil(size(curData.time{1},2)/30)*30/100; % padding in s, such that cfg.foi/T is integer
        cfg.foi          = [28 30 32];            
        tmpData = ft_freqanalysis(cfg, curData);
        MTMdata.powspctrm(:,:,indWindow,:) = tmpData.powspctrm;
        MTMdata.dimord = 'rpt_chan_time_freq';
        MTMdata.time(indWindow) = mean([timeWindows(indWindow) timeWindows(indWindow)+timeWindow]);
    end
    MTMdata.label = tmpData.label;
    MTMdata.freq = tmpData.freq;
    MTMdata.cfg = tmpData.cfg;
    clear tmpData;

    % spectral correction using adjacent frequency bands

    MTMdata.powspctrm_norm = squeeze(MTMdata.powspctrm(:,:,:,2)-nanmean(MTMdata.powspctrm(:,:,:,[1 3]),4));
    %MTMdata.powspctrm_norm = squeeze(MTMdata.powspctrm(:,1:60,:,2));

    for indTrial = 1:size(MTMdata.powspctrm_norm,1)
       data.trial{indTrial} = squeeze(MTMdata.powspctrm_norm(indTrial,:,:));
    end
   
    %% assign trials according to combinatorials
    
    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix

    % get all 16 unique sequences
    combs = allcomb([1,2],[1,2],[1,2],[1,2]);
    % get indication for the winning options for all trials still in the EEG recording
    curTrlWins = squeeze(winOptions.winningOptions(:, idx_win, data.TrlInfo(:,7)));
    % for each trial, assign combination ID
    combID = [];
    for indTrial = 1:size(curTrlWins,2)
        combID(indTrial) = find(ismember(combs, curTrlWins(:,indTrial)', 'rows'));        
    end
    
    D = NaN(16,16,size(data.trial{1},1),size(data.trial{1},2)); % condition*trial*channel*time matrix
    for indComb = 1:size(combs,1) % for each attribute sort by winning and non-winning choice 
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indComb));
        includedTrials = find(combID==indComb);
        numIncluded(indComb) = numel(includedTrials);
        for indTrial = 1:numIncluded(indComb)
            trialSize = size(data.trial{includedTrials(indTrial)},2);
            D(indComb, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
        end
    end

    %% univariate noise normalization

    %baseline_time_points=find(data.time{1}>-1, 1, 'first'):find(data.time{1}<-.5, 1, 'last'); % pre-cue interval baseline

    M=size(D,1);
    N=size(D,2);
    O=size(D,3);
    T=size(D,4);
% 
%     for indAtt = 1:16
%         baseline_mean=nanmean(C(indAtt,:,:,baseline_time_points),4);
%         baseline_std=nanstd(C(indAtt,:,:,baseline_time_points),[],4);
%         D=(C-baseline_mean)./baseline_std;
%     end; clear C;

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we run 4 separate decodings.
    
    num_permutations=100;
    DA=NaN(1,num_permutations,M,M,T);
        for perm =1:num_permutations
            tic
                        
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min(numIncluded);
            permutedD = NaN(16,subsetAmount,O,T);
            for indCond = 1:16
                trialsAvailable = find(~isnan(D(indCond,:,1,1)));
                trialsSelected = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                permutedD(indCond,:,:,:) = D(indCond,trialsSelected,:,:);
            end

            %% perform SVM classification
            
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(permutedD(condA,1:floor(.75*subsetAmount),:,time_point)) ; squeeze(permutedD(condB,1:floor(.75*subsetAmount),:,time_point))];
                        MEEG_testing_data=[squeeze(permutedD(condA,floor(.75*subsetAmount)+1:end,:,time_point)) ; squeeze(permutedD(condB,floor(.75*subsetAmount)+1:end,:,time_point))];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,size(MEEG_training_data,1)/2) 2*ones(1,size(MEEG_training_data,1)/2)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test=[ones(1,size(MEEG_testing_data,1)/2) 2*ones(1,size(MEEG_testing_data,1)/2)];%[1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(1,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
%figure; plot(squeeze(nanmean(DA(1,:,1,2,:),2)))

    
    % average across permutations
    DA_end = nanmean(DA,2);
    DA_std = nanstd(DA,[],2);
    
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1:4,1,2,:),2),3),1)));
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1,:,:,:,:),4),3),1)));

    %% save results

    save([pn.out, ID, '_Decode16Comb_SSVEP.mat'], 'DA_end', 'DA_std');
    
end % function end