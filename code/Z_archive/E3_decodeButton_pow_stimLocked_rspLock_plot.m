% Plot results for decoding button press

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/F_L10/';

% load behavioral data
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeButton_pow_stimLocked_L1O.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_end(:,:,:,1:end-10);
        % smooth for plotting (stats are based on original data)
        % smooth via moving mean average filter across 10 samples
        DA_merged{indAge}(id,:,:,:,:) = smoothdata(DA_merged{indAge}(id,:,:,:,:),5, 'movmean', 10);
    end
end

%% plot results

time = 2:0.01:8;

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .5 .2]);
set(gcf,'renderer','Painters')
subplot(1,3,[2,3]); cla;
hold on;
patches.timeVec = [3 6]-3;
patches.colorVec = [.9 .9 .9];
for indP = 1:size(patches.timeVec,2)-1
    YLim = [47 54];
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end
for indCond = 1:4
    condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:),3),4),2));
    curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,indCond,:,:,:),3),4));
    %curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indCond} = shadedErrorBar(time-3,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
end
ylabel('Decoding accuracy (%)')
xlabel('Time from stimulus presentation')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

% add stats results

load([pn.data, 'E_stats_decodeButton_stimLock.mat'], 'stat_1', 'stat_2','stat_3','stat_4');
for indCond = 1:4
    curmask = double(eval(['stat_', num2str(indCond),'.mask']));
    curmask(curmask==0)=NaN;
    plot(time-3, curmask*47-.25*indCond, 'LineWidth', 3, 'color', cBrew(indCond,:)); 
end
ylim([45.5 59])
xlim([-.5 5])

legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine], ...
        {'All trials'; '1 Target'; '2+ Targets, convergence'; '2+ Targets, non-convergence'}, 'location', 'NorthWest')
legend('boxoff');

%% add rsp-locked

DA_merged_rsp = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeButton_pow_rspLocked_L1O.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged_rsp{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged_rsp{indAge}(id,:,:,:,:) = DA_press_end;
    end
end
    
time = -100*.01:.01:.01*10;

subplot(1,3,1); cla;
hold on;
for indCond = 1:4
    condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged_rsp{indAge}(:,1:4,:,:,:),3),4),2));
    curData = squeeze(nanmean(nanmean(DA_merged_rsp{indAge}(:,indCond,:,:,:),3),4));
    %curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indCond} = shadedErrorBar(time,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
end
xlim([-.35 .1])
% legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine], ...
%         {'All trials'; '1 Target'; '2+ Targets, convergence'; '2+ Targets, non-convergence'}, 'location', 'NorthWest')
% legend('boxoff');
ylabel('Decoding accuracy (%)')
xlabel('Time from response')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

% add stats results

load([pn.data, 'E_stats_decodeButton_rspLock.mat'], 'stat_1', 'stat_2','stat_3','stat_4');
for indCond = 1:4
    curmask = double(eval(['stat_', num2str(indCond),'.mask']));
    curmask(curmask==0)=NaN;
    plot(time, curmask*47-.25*indCond, 'LineWidth', 3, 'color', cBrew(indCond,:)); 
end

line([0 0], [48 69], 'LineStyle', ':', 'Color', [.3 .3 .3], 'LineWidth', 2)

pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/C_figures/';
figureName = 'E_decodeButton';

saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

