function D_decodeButton_pow(indID)

    % inputs:   ID   | indicator for ID list below

    % For each trial, each feature is defined by one of two options
    % Try to decode the objectively winning option on a single trial basis
    % For each target level, train and test for each feature across all trials
    % This tests: within target level, is it possible to decode winning feature?
    % In load 1 this should only be possible if it is contained in feature is contained in the attentional set
    % In load 4 this should always be possible
    
    % Follow up: decode for in- vs. out-of-set by target load (but: maximum 16 trials available)
    % alternative: train on L1 in-set, test on all other conditions
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % perform a TFR, get frequency bins, downsample
    
    cfg = [];
    cfg.method     = 'wavelet';
    cfg.width      = 7;
    cfg.output     = 'pow';
    cfg.foi        = 8:1:15;
    cfg.toi        = 4:0.01:7.5;
    cfg.keeptrials = 'yes';
    TFRwave = ft_freqanalysis(cfg, data);
    
    % average in freq ranges of interest
    
    freqRanges = [8,25];
    for indFreq = 1:numel(freqRanges)-1
        curFreq = TFRwave.freq >= freqRanges(indFreq) & TFRwave.freq<freqRanges(indFreq+1);
        TFRwave.powavg(:,:,indFreq,:) = squeeze(nanmean(TFRwave.powspctrm(:,:,curFreq,:),3));
    end

    timeWindows = 4:0.01:7.5;    
    for indTrial = 1:numel(data.trial)
        [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
        data.trial{indTrial} = data.trial{indTrial}(:,tmpPoint);
    end

    % concatenate power values for decoding
    for indTrial = 1:numel(data.trial)
        data.trial{indTrial} = cat(1, zscore(data.trial{indTrial},[],1), ...
            zscore(squeeze(TFRwave.powavg(indTrial,:,1,:)),[],1));
    end
    
    %% create indicator of button press left vs. right
    
    % identify left/right response for each trial
    behavData = load([pn.data, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat']);
    leftRightVector = behavData.MergedDataEEG.TargetOption(data.TrlInfo(:,7),... % 1 = left; 2 = right
        find(strcmp(behavData.IDs_all, ID)));
    
    Accs = behavData.MergedDataEEG.Accs(data.TrlInfo(:,7),find(strcmp(behavData.IDs_all, ID)));
    
    % Define trials based on the executed motor response (not necessarily
    % the correct choice).
    
    % determine which response was given
    for indTrial = 1:numel(leftRightVector)
        if leftRightVector(indTrial) == 1 & Accs(indTrial) == 1 % correct resp was left, correct resp, resp = left
            lrVector(indTrial) = 1;
        elseif leftRightVector(indTrial) == 1 & Accs(indTrial) == 0 % correct resp was left, incorrect resp, resp = right
            lrVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 1 % correct resp was right, correct resp, resp = right
            lrVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 0 % correct resp was right, incorrect resp, resp = left
            lrVector(indTrial) = 1;
        end
    end
        
    %% arrange trials by dimensionality and left/right response

    for indDim = 1:4 % for each attribute sort by winning and non-winning choice 
        D{indDim} = NaN(2,128,size(data.trial{1},1),size(data.trial{1},2)); % condition*trial*channel*time matrix
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indDim));
        % extract (here: left and right) options for trials available in the EEG data
        curTrialLR = squeeze(lrVector');
        for indOption = 1:2
            includedTrials = find(data.TrlInfo(:,8) == indDim & curTrialLR==indOption);
            numIncluded = numel(includedTrials);
            for indTrial = 1:numIncluded
                trialSize = size(data.trial{includedTrials(indTrial)},2);
                D{indDim}(indOption, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    M=size(D{1},1);
    N=size(D{1},2);
    O=size(D{1},3);
    T=size(D{1},4);

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we run 4 separate decodings.
    
    num_permutations=100;
    DA=NaN(indDim,num_permutations,M,M,T);
    for indDim = 1:4
        for perm =1:num_permutations
            tic
            
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min([numel(find(~isnan(D{indDim}(1,:,1,1)))); numel(find(~isnan(D{indDim}(2,:,1,1))))]);;
            permutedD{indDim} = NaN(2,subsetAmount,O,T);
            for indCond = 1:2
                trialsAvailable = find(~isnan(D{indDim}(indCond,:,1,1)));
                trialsSelected = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                permutedD{indDim}(indCond,:,:,:) = D{indDim}(indCond,trialsSelected,:,:);
            end

            %% bin data into L=N/K pseudo trials
            
            K=1;
            L=subsetAmount/K;
            pseudo_trialD{indDim}=NaN(M,L,O,T);
            for step=1:L %average by steps
                trial_selector=(1+(step-1)*K):(K+(step-1)*K); %select trials to be averaged
                pseudo_trialD{indDim}(:,step,:,:)= nanmean(permutedD{indDim}(:,trial_selector,:,:),2); %assign pseudo trial to pseudo_trial_D
            end

            %% perform SVM classification
            
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(pseudo_trialD{indDim}(condA,1:floor(.75*subsetAmount),:,time_point)) ; squeeze(pseudo_trialD{indDim}(condB,1:floor(.75*subsetAmount),:,time_point))];
                        MEEG_testing_data=[squeeze(pseudo_trialD{indDim}(condA,floor(.75*subsetAmount)+1:end,:,time_point)) ; squeeze(pseudo_trialD{indDim}(condB,floor(.75*subsetAmount)+1:end,:,time_point))];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,size(MEEG_training_data,1)/2) 2*ones(1,size(MEEG_training_data,1)/2)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test=[ones(1,size(MEEG_testing_data,1)/2) 2*ones(1,size(MEEG_testing_data,1)/2)];%[1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indDim,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
    end % target attribute loop
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
%     figure; 
%     hold on; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1,1,2,:),2),3),1)));
%     plot(squeeze(nanmean(nanmean(nanmean(DA_end(2:4,1,2,:),2),3),1)));

    %% save results

    save([pn.out, ID, '_DecodeButton_pow.mat'], 'DA_end', 'DA_std');
    
end % function end