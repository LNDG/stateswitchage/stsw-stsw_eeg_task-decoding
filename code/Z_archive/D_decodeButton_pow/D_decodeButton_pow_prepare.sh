#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/fieldtrip/')
ft_defaults()
ft_compile_mex(true)
% add libsvm toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/libsvm-3.11/')
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/A_scripts/D_decodeButton_pow/')
%% compile function and append dependencies
mcc -m D_decodeButton_pow.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/libsvm-3.11
exit