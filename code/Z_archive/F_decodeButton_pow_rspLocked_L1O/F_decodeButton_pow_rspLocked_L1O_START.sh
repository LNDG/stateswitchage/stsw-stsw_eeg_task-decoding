#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/F_decodeButton_pow_rspLocked_L1O

subjectAmount=47
for i in $(seq 1 $subjectAmount); do
	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name STSWD_DecPress_pow_L1O_${i}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 2"				>> job.slurm
	echo "#SBATCH --mem 15gb" 						>> job.slurm
	echo "#SBATCH --time 15:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/STSWD_DecPress_${i}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./F_decodeButton_pow_rspLocked_L1O_run.sh /opt/matlab/R2016b $i" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done