#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% add fieldtrip toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/fieldtrip/')
ft_defaults()
ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/A_scripts/J6_decodeWinOption_lasso_v9/')
%% compile function and append dependencies
mcc -m J6_decodeWinOption_lasso_v9.m
exit