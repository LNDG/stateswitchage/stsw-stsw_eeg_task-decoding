% v7: decode win option for each feature
% v8: apply also on higher loads

function J6_decodeWinOption_lasso_v9(indID)

    % inputs:   ID   | indicator for ID list below

    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/J_lassoRegression/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};
    %ID = '1219'

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
     %% CSD transform
    
    TrlInfo = data.TrlInfo;
    
    csd_cfg = [];
    csd_cfg.elecfile = [pn.root, 'T_tools/fieldtrip/template/electrode/standard_1005.elc'];
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    
    % 25 Hz low-pass
    
    TrlInfo = data.TrlInfo;
    
    cfg = [];
    cfg.lpfilter      = 'yes';
    cfg.lpfreq        = [25];
    cfg.lpfiltord     = 6;
    cfg.lpfilttype    = 'but';
    [data] = ft_preprocessing(cfg, data);
    data.TrlInfo = TrlInfo;
    
    % downsample to 50 Hz
    
    cfg = [];
    cfg.resamplefs = 50;
    cfg.detrend = 'yes';
    cfg.demean = 'yes';
    [data] = ft_resampledata(cfg, data);
    
    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix

    D = NaN(4,2,128,60,size(data.trial{1},2)); % condition*trial*channel*time matrix
    for indAtt = 1:4 % for each attribute sort by winning and non-winning choice 
        %D{indAtt} = NaN(2,128,60,size(data.trial{1},2)); % condition*trial*channel*time matrix
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indAtt));
        % extract winning and missing options for trials available in the EEG data
        curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(:,7)));
        for indOption = 1:2
            %includedTrials = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == 1 & curTrialWinMiss==indOption);
            includedTrials = find(curTrialWinMiss==indOption);
            numIncluded = numel(includedTrials);
            for indTrial = 1:numIncluded
                trialSize = size(data.trial{includedTrials(indTrial)},2);
                %D{indAtt}(indOption, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
                D(indAtt, indOption, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    %% 3) Decoding

    % get minimum requirements
    for indAtt = 1:4
        for indOption = 1:2
            trialsAvailable{indAtt,indOption} = find(~isnan(D(indAtt, indOption,:,1,1)));
            trialsAvailableN(indAtt,indOption) = numel(trialsAvailable{indAtt,indOption});
        end
    end
    Ntrials = cellfun(@numel,trialsAvailable);
    info.NleftIn = min(min(Ntrials-1));
    info.NleftOut = Ntrials-info.NleftIn;
    
    Nfolds=30;
    
    timeToTrain = [find(data.time{1}>3.18 & data.time{1}<3.22)];
    timeToTrain = timeToTrain(1:2:end);
    probabilityCourse = NaN(Nfolds,4,16,2,numel(timeToTrain),size(data.time{1},2));
    accuracy = NaN(Nfolds,4,16,numel(timeToTrain),size(data.time{1},2));
    for perm =1:Nfolds
        weights = [];
        disp(['Permutation ', num2str(perm), ' of ', num2str(Nfolds)]);
        % designate in- and out- trials
        % note that these are true for all trained models!
        randTrials = randperm(min(min(Ntrials)));
        trialsIn = randTrials(1:info.NleftIn);
        trialsOut = randTrials(info.NleftIn+1:end);
        
        for indTime =1:numel(timeToTrain)
            time_point = timeToTrain(indTime);
            for model=1:4 % each model should represent a mix of in-set and out-of -set
                % create leave- in and leave-out set plus labels
                MEEG_training_data = []; MEEG_test_data = []; LabelsIn = []; LabelsOut = [];
                % add training data
                % add option 1 as 0(incorrect), option 2 as 1 (correct)
                tmp_data = squeeze(D(model,1, trialsIn,:,time_point));
                MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                LabelsIn = cat(1,LabelsIn, repmat(0,size(tmp_data,1),1));
                tmp_data = squeeze(D(model,2, trialsIn,:,time_point));
                MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                LabelsIn = cat(1,LabelsIn, repmat(1,size(tmp_data,1),1));
                Lp = 7*10^-3;
                [tmpWeights, statsInfo] = lassoglm(MEEG_training_data, LabelsIn,'binomial','Alpha', 1, 'Lambda', Lp);
                weights(:,model,indTime) = [tmpWeights; statsInfo.Intercept];
            end
            % test on left-out samples
            % get probability of model 1:4
            
            for indAtt = 1:4
                for indOption = 1:2
                    curSignal = D(indAtt, indOption, trialsOut,:,:);
                    for indTrial = 1:numel(trialsOut)
                        trlSignal = squeeze(curSignal(1,1,indTrial,:,:));
                        for indTime2 = 1:size(trlSignal,2)
                            model = indAtt;
                            % prediction model: intercept + beta*sensor
                            curPred = weights(end,model,indTime) + weights(1:end-1,model,indTime)'*trlSignal(:,indTime2);
                            curPred = 1./(1 + exp(-1.*(curPred-0)));
                            probabilityCourse(perm,indAtt,indTrial,indOption,indTime,indTime2) = curPred;
                            if curPred > .5
                                curPred = 2;
                            elseif curPred < .5
                                curPred = 1;
                            else curPred = 0;
                            end
                            if curPred == indOption
                                accuracy(perm,indAtt,indTrial,indTime,indTime2) = 1;
                            else
                                accuracy(perm,indAtt,indTrial,indTime,indTime2) = 0;
                            end
                        end
                    end
                end
            end
        end % end of model training
        % save mean model weights
        info.EEGweights(perm,:,:) = squeeze(nanmean(weights,3));
    end % permutation
    
    %% prepare outputs

    info.probabilityCourse = [];
    info.probabilityCourse = squeeze(nanmean(nanmean(nanmean(probabilityCourse,5),3),1)); % state * model * time
    info.accuracy = [];
    info.accuracy = squeeze(nanmean(nanmean(nanmean(accuracy,4),3),1)); % state * time
    info.accuracy_time = [];
    info.accuracy_time = squeeze(nanmean(nanmean(nanmean(accuracy,3),2),1));

%     figure; imagesc(data.time{1},[],info.accuracy)
%	figure; plot(data.time{1},smoothts(squeeze(nanmean(info.accuracy,1)),'b',10))
%     figure; plot(data.time{1},squeeze(nanmean(info.accuracy,1)))
%     figure; imagesc(data.time{1}, data.time{1}(timeToTrain),squeeze(nanmean(nanmean(nanmean(accuracy,3),2),1)))
%     figure; imagesc(squeeze(nanmean(info.probabilityCourse,2)))
    
    %% apply decoder on all unlabeled trials to decode attended attributes
    
    decoded.decodedAttributes = []; decoded.decodedAccuracy = [];
    for indTrial = 1:size(data.trial,2)
        for indTime = 1:size(data.trial{indTrial},2)
            for indDecoder = 1:4
                % apply decoder avg. across permutations (already defines across trials and avg. across temporally-specific decoders)
                curPred = nanmean(info.EEGweights(:,end,indDecoder),1) +...
                    data.trial{indTrial}(:,indTime)'*squeeze(nanmean(info.EEGweights(:,1:end-1,indDecoder),1))';
                curPred = 1./(1 + exp(-1.*(curPred-0)));
                % turn into categorical predictions
                if curPred > .5
                    curPred = 2;
                elseif curPred < .5
                    curPred = 1;
                else curPred = 0;
                end                
                decoded.decodedAttributes(indTrial,indDecoder,indTime) = curPred;
                % indicate accuracy
                curTrueOption = squeeze(winOptions.winningOptions(indDecoder, idx_win, data.TrlInfo(indTrial,7)));
                decoded.decodedAccuracy(indTrial,indDecoder,indTime) = double(curPred==curTrueOption);
            end
        end
    end
    
    decoded.TrlInfo = data.TrlInfo;
    
%     figure; plot(squeeze(nanmean(nanmean(decoded.decodedAccuracy(decoded.TrlInfo(:,8)==4,2,:),2),1)))
%     figure; plot(squeeze(nanmean(nanmean(decoded.decodedAttributes(1,2,:),2),1)))

    %% save outputs

    save([pn.out, ID, '_logRegressionDecode_v9.mat'], 'info', 'decoded')

end % function end