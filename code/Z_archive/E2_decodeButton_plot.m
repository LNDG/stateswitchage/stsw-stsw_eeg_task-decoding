% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

% load behavioral data
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeButton_pow_rspLocked.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:,:,:) = NaN;
            continue;
        end
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_press_end;
        %DA_merged{indAge}(id,:,:,:,:) = DA_end;
    end
end

%% plot results
    
time = -100*.01:.01:.01*10;

for indAge = 1%:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(DA_merged{indAge}(:,1,:,:,:));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(DA_merged{indAge}(:,2,:,:,:));
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = squeeze(DA_merged{indAge}(:,3,:,:,:));
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = squeeze(DA_merged{indAge}(:,4,:,:,:));
end

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
    
% h = figure;
% hold on;
% for indCond = 1:4
%     subplot(4,1,indCond)
%     condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:),3),4),2));
%     curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,indCond,:,:,:),3),4));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     dimPlot{indCond} = shadedErrorBar(time,squeeze(nanmean(curData,1)),squeeze(standError), ...
%         'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
% end

h = figure;
hold on;
for indCond = 1:4
    condAvg = squeeze(nanmean(nanmean(nanmean(DA_merged{indAge}(:,1:4,:,:,:),3),4),2));
    curData = squeeze(nanmean(nanmean(DA_merged{indAge}(:,indCond,:,:,:),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indCond} = shadedErrorBar(time,squeeze(nanmean(curData,1)),squeeze(standError), ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
end
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine], ...
        {'All trials'; '1 Target'; '2+ Targets, convergence'; '2+ Targets, non-convergence'}, 'location', 'NorthWest')
    
figure; imagesc(condAvg)
figure; imagesc(curData)
