
function I4B_decodeWinOption_byLoad(ID)

    % Time-resolved decoding of prevalent feature
    % single timepoint training and testing
    % train across dims, test on each Dim separately (and on uncued)
    
    % TO DO:
    % adjust data input
    % implement N fold cross-validation
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        % add libsvm toolbox
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; addpath(genpath(pn.libsvm));
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
        pn.FieldTrip    = [pn.root, 'T_tools/fieldtrip/']; addpath(pn.FieldTrip); ft_defaults;
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end
    pn.data    = [pn.root, 'B_data/'];
    pn.out     = [pn.root, 'B_data/B_DecodingResults/'];

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % perform a TFR, get frequency bins, downsample
    
%     cfg = [];
%     cfg.method     = 'wavelet';
%     cfg.width      = 7;
%     cfg.output     = 'pow';
%     cfg.foi        = 8:1:15;
%     cfg.toi        = -.5:0.05:7.5;
%     cfg.keeptrials = 'yes';
%     TFRwave = ft_freqanalysis(cfg, data);
%     
%     % average in freq ranges of interest
%     
%     freqRanges = [8,25];
%     for indFreq = 1:numel(freqRanges)-1
%         curFreq = TFRwave.freq >= freqRanges(indFreq) & TFRwave.freq<freqRanges(indFreq+1);
%         TFRwave.powavg(:,:,indFreq,:) = squeeze(nanmean(TFRwave.powspctrm(:,:,curFreq,:),3));
%     end
% 
%     % get gamma
%     cfg = [];
%     cfg.method = 'mtmconvol';
%     cfg.keeptapers  = 'no';
%     cfg.taper = 'dpss'; % high frequency-optimized analysis (smooth)
%     cfg.foi = 40:20:140;
%     cfg.tapsmofrq = ones(length(cfg.foi),1) .* 8;
%     cfg.t_ftimwin = ones(length(cfg.foi),1) .* 0.4;
%     cfg.toi = -.5:0.05:7.5;
%     cfg.keeptrials = 'yes';
%     cfg.pad = 'nextpow2';
%     Gamma = ft_freqanalysis(cfg, data);
%     
%     Gamma.powavg(:,:,:) = squeeze(nanmean(Gamma.powspctrm(:,:,:,:),3));

    % downsample broadband time series
    stepsize = 0.05; % in s
    samples = floor(stepsize/(1/data.fsample)/2);
    timeWindows = -.5:stepsize:7.5;    
    for indTrial = 1:numel(data.trial)
        [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
        for indTP = 1:numel(tmpPoint)
            data.trialavg{indTrial}(:,indTP) = ...
                nanmedian(data.trial{indTrial}(:,tmpPoint(indTP)-samples:tmpPoint(indTP)+samples),2);
        end
    end

%     % concatenate power values for decoding
%     for indTrial = 1:numel(data.trial)
%         data.trial{indTrial} = cat(1, data.trialavg{indTrial}, ...
%             squeeze(TFRwave.powavg(indTrial,:,1,:)),...
%              squeeze(Gamma.powavg(indTrial,:,:)));
%     end

    data.trial = data.trialavg;
    
    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix
    
    N_chans = size(data.trial{1},1);
    N_time = size(data.trial{1},2);
    N_conditions = 2;
    
    numIncluded = [];
    for indDim = 1:4
        for indAtt = 1:4
            disp(num2str(indAtt));
            D{indDim,indAtt} = NaN(2,8,N_chans,N_time); % condition*trial*channel*time matrix
            curTrial = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == indDim);
            curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(curTrial ,7)));
            for indOption = 1:2
                includedTrials = curTrial(curTrialWinMiss==indOption);
                numIncluded(indDim,indAtt, indOption) = numel(includedTrials);
                for indTrial = 1:numIncluded(indDim,indAtt, indOption)
                    D{indDim,indAtt}(indOption,indTrial,:,1:N_time) = data.trial{includedTrials(indTrial)}(:,:);
                end
            end
        end
    end
    
    %% 3) Decoding
    
    num_permutations=100;
    DA=NaN(4,4,num_permutations,N_conditions,N_conditions,N_time);
    for indDim = 1:4
        for indAtt = 1:4
            for perm =1:num_permutations
                tic

                % create folds such that min. 1 of each category is not
                % included in the fold

                % reserve 1 random trial for each condition that will not be
                % included in training set
                
                train = cell(2,4); % training set specific to dim 1, attribute
                for indDim_s = 1:4
                    for indAtt_s = 1:4
                        test{indDim_s,indAtt_s} = NaN(2,1,N_chans,N_time);
                        trialsTrain = [];
                        trialsTest = [];
                        for indCond = 1:2
                            trialsAvailable     = find(~isnan(D{indDim_s,indAtt_s}(indCond,:,1,1)));
                            randomTrials        = randperm(numel(trialsAvailable));
%                             trialsTrain         = trialsAvailable(randomTrials(1:numel(trialsAvailable)-1));
%                             trialsTest          = trialsAvailable(randomTrials(end));
                            % alternative procedure: implement triadic
                            % split and averaging
                            Nfold = 3;
                            Nperfold = floor(numel(trialsAvailable)/Nfold);
                            trialsTrain{1} = randomTrials(1:Nperfold);
                            trialsTrain{2} = randomTrials(Nperfold+1:2*Nperfold);
                            trialsTest = randomTrials(2*Nperfold+1:end);
                            train{indCond,indAtt_s} = cat(2, train{indCond,indAtt_s}, ...
                                nanmean(D{indDim_s,indAtt_s}(indCond,trialsTrain{1},:,:),2),...
                                nanmean(D{indDim_s,indAtt_s}(indCond,trialsTrain{2},:,:),2));
                            test{indDim_s,indAtt_s}(indCond,:,:,:) = nanmean(D{indDim_s,indAtt_s}(indCond,trialsTest,:,:),2);
                        end
                    end
                end
                
                % equalize number of training trials
                
                tmp_size = [];
                for indAtt_s = 1:4
                    for indCond = 1:2
                        tmp_size = cat(1, tmp_size, size(train{indCond,indAtt_s},2));
                    end
                end
                L = min([tmp_size]);

                %% perform SVM classification

                for condA=1:N_conditions %loop through all conditions
                    for condB = condA+1:N_conditions  %loop through all conditions >condA+1
                        for time_point =1:N_time % all time points are independent
                            % L-1 pseudo trials go to testing set, the Lth to training set
                            MEEG_training_data=[squeeze(train{condA,indAtt}(1,1:L,:,time_point)) ; squeeze(train{condB,indAtt}(1,1:L,:,time_point))];
                            MEEG_testing_data=[squeeze(test{indDim,indAtt}(condA,1,:,time_point))' ; squeeze(test{indDim,indAtt}(condB,1,:,time_point))'];
                            % you have to define class labels for the trials you will train and test
                            labels_train=[ones(1,L) 2*ones(1,L)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                            labels_test= [1 2]; % you will test on one trial from condA, and one from cond B
                            % train SVM
                            model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                            % test SVM
                            [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                            DA(indDim,indAtt,perm,condA,condB,time_point)=accuracy(1);
                        end % time point
                    end % cond A
                end %cond B
                toc
            end % permutation
        end % attribute loop
    end
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,3));
    DA_std = squeeze(nanstd(DA,[],3));
    
    %figure; plot(squeeze(DA_end(1,1,1,2,:)))
   
    %% save results

    save([pn.out, ID, '_I4B_decodeWinOption_byLoad.mat'], 'DA_end', 'DA_std');
    
end % function end