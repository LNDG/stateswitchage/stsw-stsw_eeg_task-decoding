% sanity-check ERPs left vs. right motion

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};


   if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        % add libsvm toolbox
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; addpath(genpath(pn.libsvm));
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end
    pn.data    = [pn.root, 'B_data/'];
    pn.out     = [pn.root, 'B_data/B_DecodingResults/'];

    for indID = 1:numel(IDs)
        ID = IDs{indID};

        % load EEG data
        load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])

        % downsample broadband time series
        timeWindows = -.5:0.01:7.5;    
        for indTrial = 1:numel(data.trial)
            [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
            data.trial{indTrial} = data.trial{indTrial}(:,tmpPoint);
        end

        load([pn.data, 'A_winOptions.mat'], 'winOptions')
        idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix

        N_chans = 60;
        N_time = size(data.trial{1},2);
        N_conditions = 2;

        numIncluded = [];
        for indDim = 1:4
            for indAtt = 1:4
                disp(num2str(indAtt));
                D{indDim,indAtt} = NaN(2,8,N_chans,N_time); % condition*trial*channel*time matrix
                curTrial = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == indDim);
                curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(curTrial ,7)));
                for indOption = 1:2
                    includedTrials = curTrial(curTrialWinMiss==indOption);
                    numIncluded(indDim,indAtt, indOption) = numel(includedTrials);
                    for indTrial = 1:numIncluded(indDim,indAtt, indOption)
                        D{indDim,indAtt}(indOption,indTrial,:,1:N_time) = data.trial{includedTrials(indTrial)}(:,:);
                    end
                end
            end
        end

        for indDim = 1:4
            for indAtt = 1:4
                tmpData = squeeze(nanmean(D{indDim,indAtt},2));
                OptionDifference(indID,indDim,indAtt,:,:) = squeeze(tmpData(1,:,:)-tmpData(2,:,:));
            end
        end
    end
    
    figure; 
    imagesc(data.time{1},[],squeeze(nanmean(OptionDifference(:,1,2,:,:),1)))
    imagesc(data.time{1},[],squeeze(nanmean(nanmean(nanmean(OptionDifference(:,1:4,1:4,:,:),3),2),1)))
    imagesc(data.time{1},[],squeeze(nanmean(OptionDifference(:,1,4,:,:),1)))
    imagesc(data.time{1},[],squeeze(nanmean(OptionDifference(:,1,2,:,:),1)))
    imagesc(data.time{1},[],squeeze(nanmean(OptionDifference(:,4,2,:,:),1)))
    