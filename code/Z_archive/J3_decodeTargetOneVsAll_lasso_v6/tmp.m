function J2_decodeTargetOneVsAll_lasso_v5(indID)

    % inputs:   ID   | indicator for ID list below

    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/J_lassoRegression/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};
    %ID = '1219'

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % filter to 28 to 32 Hz range
    
    TrlInfo = data.TrlInfo;
    
    cfg = [];
    cfg.lpfilter      = 'yes';
    cfg.lpfreq        = [20];
    cfg.lpfiltord     = 6;
    cfg.lpfilttype    = 'but';
    [data] = ft_preprocessing(cfg, data);
    data.TrlInfo = TrlInfo;
    
    %downsample
    
    smoothVector = 1:5:size(data.trial{1},2);
    for indTrial = 1:numel(data.trial)
        data.trialMean{indTrial} = data.trial{indTrial}(:,smoothVector);
        data.timeResamp = data.time{1}(smoothVector);
    end

%     smoothVector = 1:5:size(data.trial{1},2);
%     for indTrial = 1:numel(data.trial)
%         for indSmooth = 1:numel(smoothVector)-1
%             data.trialMean{indTrial}(:,indSmooth) = squeeze(nanmean(data.trial{indTrial}(:,smoothVector(indSmooth):smoothVector(indSmooth+1)),2));
%         end
%     end
%     % align time vector
%     for indSmooth = 1:numel(smoothVector)-1
%         data.timeResamp(indSmooth) = nanmean(data.time{1}(smoothVector(indSmooth):smoothVector(indSmooth+1)));
%     end
    
    numofAtts = 1;
    D = NaN(4,16,60,size(data.timeResamp,2)); % condition*trial*channel*time matrix
    for indAtt = 1:4
        disp(num2str(indAtt));    
        includedTrials = find(data.TrlInfo(:,8) == numofAtts & data.TrlInfo(:,6) == indAtt);
        numIncluded = numel(includedTrials);
        for indTrial = 1:numIncluded
            trialSize = size(data.trialMean{includedTrials(indTrial)},2);
            D(indAtt,indTrial,:,1:trialSize) = data.trialMean{includedTrials(indTrial)};
        end
    end

    %% 3) Decoding

    % get minimum requirements
    for indAtt = 1:4
        trialsAvailable{indAtt} = find(~isnan(D(indAtt,:,1,1)));
    end
    Ntrials = cellfun(@numel,trialsAvailable);
    info.NleftIn = min(Ntrials-1);
    info.NleftOut = Ntrials-info.NleftIn;
    
    Nfolds=30;

    timeToTrain = [find(data.timeResamp>3.18 & data.timeResamp<3.25)];
    timeNotToTrain = [find(data.timeResamp>0.18 & data.timeResamp<0.21)];
    timeNotToTrain2 = [find(data.timeResamp>2.98 & data.timeResamp<3)];
    probabilityCourse = NaN(Nfolds,4,16,4,numel(timeToTrain),1100);
    accuracy = NaN(Nfolds,4,16,numel(timeToTrain),1100);
    for perm =1:Nfolds
        weights = [];
        disp(['Permutation ', num2str(perm), ' of ', num2str(Nfolds)]);
       % designate in- and out- trials
       % note that these are true for all trained models!
        for indAtt = 1:4
            randTrials = randperm(numel(trialsAvailable{indAtt}));
            trialsIn{indAtt} = randTrials(1:info.NleftIn);
            trialsOut{indAtt} = randTrials(info.NleftIn+1:end);
        end
        for indTime =1:numel(timeToTrain)
            time_point = timeToTrain(indTime);
            for model=1:4 % each model should represent a mix of in-set and out-of -set
                % create leave- in and leave-out set plus labels
                MEEG_training_data = []; MEEG_test_data = []; LabelsIn = []; LabelsOut = [];
                for indAtt = 1:4
                    % add training data
                    tmp_data = squeeze(D(indAtt, trialsIn{indAtt},:,time_point));
                    MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                    LabelsIn = cat(1,LabelsIn, repmat(indAtt==model,size(tmp_data,1),1));
                    % add antagonistic examples
                    tmp_data = squeeze(D(indAtt, trialsIn{indAtt},:,timeNotToTrain(1)));
                    MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                    LabelsIn = cat(1,LabelsIn, repmat(0,size(tmp_data,1),1));
                    tmp_data = squeeze(D(indAtt, trialsIn{indAtt},:,timeNotToTrain2(1)));
                    MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                    LabelsIn = cat(1,LabelsIn, repmat(0,size(tmp_data,1),1));
                end
                Lp = .003;%Lp = 7*10^-3;
                [tmpWeights, statsInfo] = lassoglm(MEEG_training_data, LabelsIn,'binomial','Alpha', 1, 'Lambda', Lp);
                weights(:,model,indTime) = [tmpWeights; statsInfo.Intercept];
            end
            % test on left-out samples
            % get probability of model 1:4

            for indAtt = 1:4
                curSignal = D(indAtt, trialsOut{indAtt},:,:);
                for indTrial = 1:numel(trialsOut{indAtt})
                    trlSignal = squeeze(curSignal(1,indTrial,:,:));
                    for indTime2 = 1:size(trlSignal,2)
                        for model = 1:4
                            % prediction model: intercept + beta*sensor
                            % MATLAB-specific: 
                            %coef = [weights(end,model,indTime); weights(1:end-1,model,indTime)];
                            %curPred = glmval(coef, trlSignal(:,indTime2)', 'logit')
                            curPred = weights(end,model,indTime) + weights(1:end-1,model,indTime)'*trlSignal(:,indTime2);
                            curPred = 1./(1 + exp(-1.*(curPred-0)));
                            probabilityCourse(perm,indAtt,indTrial,model,indTime,indTime2) = curPred;
                        end
                        % calculate accuracy
                        [~, tmp_discreteCourse] = max(probabilityCourse(perm,indAtt,indTrial,:,indTime,indTime2),[],4);
                        if tmp_discreteCourse == indAtt
                        accuracy(perm,indAtt,indTrial,indTime,indTime2) = 1;
                        else
                        accuracy(perm,indAtt,indTrial,indTime,indTime2) = 0;
                        end
                    end
                end
            end
        end % end of model training
        % save mean model weights
        info.EEGweights(perm,:,:) = squeeze(nanmean(weights,3));
    end % permutation
        
    %% prepare outputs

    info.probabilityCourse = [];
    info.probabilityCourse = squeeze(nanmean(nanmean(nanmean(probabilityCourse,5),3),1)); % state * model * time
    info.accuracy = [];
    info.accuracy = squeeze(nanmean(nanmean(nanmean(accuracy,4),3),1)); % state * time

%     figure; imagesc(data.timeResamp,[],info.accuracy)
%	figure; plot(data.timeResamp,smoothts(squeeze(nanmean(info.accuracy,1)),'b',10))
%     figure; plot(data.timeResamp,squeeze(nanmean(info.accuracy,1)))
%     figure; imagesc(data.timeResamp, data.timeResamp(timeToTrain),squeeze(nanmean(nanmean(nanmean(accuracy,3),2),1)))
%     figure; imagesc(squeeze(nanmean(info.probabilityCourse,2)))
    
    %% apply decoder on all unlabeled trials to decode attended attributes
    
    decodedAttributes = []; attributeLikelihood = [];
    for indTrial = 1:size(data.trial,2)
        for indTime = 1:size(data.trialMean{indTrial},2)
            for indDecoder = 1:4
                % apply decoder avg. across permutations (already defines across trials and avg. across temporally-specific decoders)
                curPred = nanmean(info.EEGweights(:,end,indDecoder),1) +...
                    data.trialMean{indTrial}(:,indTime)'*squeeze(nanmean(info.EEGweights(:,1:end-1,indDecoder),1))';
                curPred = 1./(1 + exp(-1.*(curPred-0)));
                attributeLikelihood(indTrial,indDecoder,indTime) = curPred;
            end
            % turn into categorical predictions
            [~, decodedAttributes(indTrial,indTime)] = max(squeeze(attributeLikelihood(indTrial,:,indTime)));
        end
    end
    
%     figure; imagesc(decodedAttributes)
    
    %% quantify attention paid to in- vs. out-of-set
        
    info.OutSet = NaN(4,64); info.InSet = NaN(4,64);
    attributeSet = 1:4;
    for numofAtts = 1:4
        curTrials = find(data.TrlInfo(:,8) == numofAtts);
        for indTrial = 1:numel(curTrials)
            targetAtts = find(~isnan(data.TrlInfo(curTrials(indTrial),1:4)));
            NontargetAtts = find(~ismember(1:4, targetAtts));
            info.OutSet(numofAtts,indTrial) = numel(find(ismember(decodedAttributes(curTrials(indTrial), data.timeResamp>3 & data.timeResamp<6),NontargetAtts)))./numel(find(data.timeResamp>3 & data.timeResamp<6));
            info.InSet(numofAtts,indTrial) = numel(find(ismember(decodedAttributes(curTrials(indTrial), data.timeResamp>3 & data.timeResamp<6),targetAtts)))./numel(find(data.timeResamp>3 & data.timeResamp<6));
        end
    end
    
%     figure; bar(nanmean(info.InSet,2)-[.25, .5, .75, 1]', 'w')
     
    %% save outputs

    save([pn.out, ID, '_logRegressionDecode_v5.mat'], 'info')

end % function end