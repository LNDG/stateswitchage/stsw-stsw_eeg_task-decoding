#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/A_scripts/J2_decodeTargetOneVsAll_lasso_v5/')
%% compile function and append dependencies
mcc -m J2_decodeTargetOneVsAll_lasso_v5.m
exit