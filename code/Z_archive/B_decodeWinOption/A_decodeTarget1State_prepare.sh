#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
% add libsvm toolbox
addpath('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/D_tools/libsvm-3.11/')
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/A_scripts/A_decodeTarget1State/')
%% compile function and append dependencies
mcc -m A_decodeTarget1State.m -a /home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/D_tools/libsvm-3.11
exit