#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/"

cd ${RootPath}/A_Scripts/A_decodeTarget1State

subjectAmount=100
for i in $(seq 1 $subjectAmount); do
	echo "#PBS -N STSWD_DecTarget_${i}" 	> job
	echo "#PBS -l walltime=06:00:00" 										>> job
	echo "#PBS -l mem=5gb" 													>> job
	echo "#PBS -j oe" 														>> job
	echo "#PBS -o ${RootPath}/Y_logs/" >> job
	echo "#PBS -m n" 														>> job
	echo "#PBS -d ." 														>> job
	echo "./A_decodeTarget1State_run.sh /opt/matlab/R2016b $i " 			>> job
	qsub job
	rm job
done
