% 170926 | JQK adapted script from MRC (Cichy)

function B_decodeWinOption_v2(indID)

    % inputs:   ID   | indicator for ID list below

    % For each trial, each feature is defined by one of two options
    % Try to decode the objectively winning option on a single trial basis
    % For each target level, train and test for each feature across all trials
    % This tests: within target level, is it possible to decode winning feature?
    % In load 1 this should only be possible if it is contained in feature is contained in the attentional set
    % In load 4 this should always be possible
    
    % Follow up: decode for in- vs. out-of-set by target load (but: maximum 16 trials available)
    % alternative: train on L1 in-set, test on all other conditions
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % concatenate 4 adjacent samples in space for optimized spatial
    % decoding and temporal downsampling
    samp = 1:4:size(data.trial{1},2)-4;
    for indTrial = 1:numel(data.trial)
        data.time{indTrial} = data.time{indTrial}(samp);
        data.trial{indTrial} = nanmean(cat(3, data.trial{indTrial}(:,samp), data.trial{indTrial}(:,samp+1), ...
        data.trial{indTrial}(:,samp+2), data.trial{indTrial}(:,samp+3)),3);
    end

    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix

    for indAtt = 1:4 % for each attribute sort by winning and non-winning choice 
        C{indAtt} = NaN(2,128,60,1400); % condition*trial*channel*time matrix
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indAtt));
        % extract winning and missing options for trials available in the EEG data
        curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(:,7)));
        for indOption = 1:2
            %includedTrials = find(data.TrlInfo(:,6) == indAtt & curTrialWinMiss==indOption);
            includedTrials = find(curTrialWinMiss==indOption);
            numIncluded = numel(includedTrials);
            for indTrial = 1:numIncluded
                trialSize = size(data.trial{includedTrials(indTrial)},2);
                C{indAtt}(indOption, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    %% univariate noise normalization

    baseline_time_points=find(data.time{1}>-.5, 1, 'first'):find(data.time{1}<0, 1, 'last'); % pre-cue interval baseline

    M=size(C{1},1);
    N=size(C{1},2);
    O=size(C{1},3);
    T=size(C{1},4);

    for indAtt = 1:4
        baseline_mean=nanmean(C{indAtt}(:,:,:,baseline_time_points),4);
        baseline_std=nanstd(C{indAtt}(:,:,:,baseline_time_points),[],4);
        D{indAtt}=(C{indAtt}-baseline_mean)./baseline_std;
    end; clear C;

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we run 4 separate decodings.
    
    num_permutations=100;
    DA=NaN(indAtt,num_permutations,M,M,T);
    for indAtt = 1:4
        for perm =1:num_permutations
            tic
            % select random subset of 32 trials in random order
%             subsetAmount = 100;
%             permutedD{indAtt} = NaN(2,subsetAmount,O,T);
%             for indCond = 1:2
%                 trialsAvailable = find(~isnan(D{indAtt}(indCond,:,1,1)));
%                 % replicate random trials to fulfill required trial number
%                 resampledTrials = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount-numel(trialsAvailable)));
%                 trialsAvailable = [trialsAvailable, resampledTrials];
%                 trialsSelected = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
%                 permutedD{indAtt}(indCond,:,:,:) = D{indAtt}(indCond,trialsSelected,:,:);
%                 clear trialsAvailable trialsSelected
%             end
                        
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min([numel(find(~isnan(D{indAtt}(1,:,1,1)))); numel(find(~isnan(D{indAtt}(2,:,1,1))))]);
            permutedD{indAtt} = NaN(2,subsetAmount,O,T);
            for indCond = 1:2
                trialsAvailable = find(~isnan(D{indAtt}(indCond,:,1,1)));
                trialsSelected = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                permutedD{indAtt}(indCond,:,:,:) = D{indAtt}(indCond,trialsSelected,:,:);
            end


            %% bin data into L=N/K pseudo trials
            
            K=1;
            L=subsetAmount/K;
            pseudo_trialD{indAtt}=NaN(M,L,O,T);
            for step=1:L %average by steps
                trial_selector=(1+(step-1)*K):(K+(step-1)*K); %select trials to be averaged
                pseudo_trialD{indAtt}(:,step,:,:)= nanmean(permutedD{indAtt}(:,trial_selector,:,:),2); %assign pseudo trial to pseudo_trial_D
            end

            %% perform SVM classification
            
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(pseudo_trialD{indAtt}(condA,1:floor(.75*subsetAmount),:,time_point)) ; squeeze(pseudo_trialD{indAtt}(condB,1:floor(.75*subsetAmount),:,time_point))];
                        MEEG_testing_data=[squeeze(pseudo_trialD{indAtt}(condA,floor(.75*subsetAmount)+1:end,:,time_point)) ; squeeze(pseudo_trialD{indAtt}(condB,floor(.75*subsetAmount)+1:end,:,time_point))];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,size(MEEG_training_data,1)/2) 2*ones(1,size(MEEG_training_data,1)/2)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test=[ones(1,size(MEEG_testing_data,1)/2) 2*ones(1,size(MEEG_testing_data,1)/2)];%[1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indAtt,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
%figure; plot(squeeze(nanmean(DA(1,:,1,2,:),2)))

    end % target attribute loop
    
    % average across permutations
    DA_end = squeeze(mean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));
    
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1:4,1,2,:),2),3),1)));
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1,1,2,:),2),3),1)));

    %% save results

    save([pn.out, ID, '_DecodeTargetMultiState_v2.mat'], 'DA_end', 'DA_std');
    
end % function end