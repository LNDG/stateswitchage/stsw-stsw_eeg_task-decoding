#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/B3_decodeWinOption_SSVEP_Peak_run

subjectAmount=47
for i in $(seq 1 $subjectAmount); do
	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name STSWD_DecWin_SSVEP_${i}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 2"				>> job.slurm
	echo "#SBATCH --mem 15gb" 						>> job.slurm
	echo "#SBATCH --time 24:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/STSWD_DecWin_SSVEP_${i}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./B3_decodeWinOption_SSVEP_Peak_run.sh /opt/matlab/R2016b $i" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done