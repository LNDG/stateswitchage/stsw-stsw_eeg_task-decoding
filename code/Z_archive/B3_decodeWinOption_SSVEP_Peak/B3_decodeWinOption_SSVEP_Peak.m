% 170926 | JQK adapted script from MRC (Cichy)

function B_decodeWinOption_v2(indID)

    % inputs:   ID   | indicator for ID list below

    % For each trial, each feature is defined by one of two options
    % Try to decode the objectively winning option on a single trial basis
    % For each target level, train and test for each feature across all trials
    % This tests: within target level, is it possible to decode winning feature?
    % In load 1 this should only be possible if it is contained in feature is contained in the attentional set
    % In load 4 this should always be possible
    
    % Follow up: decode for in- vs. out-of-set by target load (but: maximum 16 trials available)
    % alternative: train on L1 in-set, test on all other conditions
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    %% extract at SSVEP peaks +- x samples
    
%     avgData = squeeze(nanmean(cat(3,data.trial{:}),3));
%     
%     data.time{1}
%     3:.033:6
%     
%     figure; plot(data.time{1},squeeze(nanmean(avgData(58:60,:),1)))
%     
%     % get samples closest to .033 ms from stimulus onset
%     
    targetTimes = 3:.033:6;
    [minVal, minIdx] = min(abs(targetTimes'-data.time{1})');
%     
%     data.time{1}(minIdx)
    
    concatData = cat(3,data.trial{:});
    
    for indRep = 1:numel(minIdx)
        dataByRep(:,indRep, :,:) = concatData(:,minIdx(indRep)-9:minIdx(indRep)+9,:);
    end
    % chan x rep x time x trials
    
%     figure; plot(squeeze(nanmean(nanmean(nanmean(dataByRep(58:60,:,:,:),4),2),1)))
%     figure; imagesc(squeeze(nanmean(nanmean(dataByRep(:,:,:,:),4),2)))
% 
%     figure; cla;
%     hold on; plot(squeeze(nanmean(nanmean(nanmean(dataByRep(58:60,:,:,curTrialWinMiss==1),4),2),1)))
%     hold on; plot(squeeze(nanmean(nanmean(nanmean(dataByRep(58:60,:,:,curTrialWinMiss==2),4),2),1)))
%     
%     figure; cla;
%     hold on; plot(squeeze(nanmean(nanmean(nanmean(dataByRep(:,:,:,curTrialWinMiss==1),4),2),3)))
%     hold on; plot(squeeze(nanmean(nanmean(nanmean(dataByRep(:,:,:,curTrialWinMiss==2),4),2),3)))
%     
%     figure; imagesc(squeeze(nanmean(nanmean(dataByRep(58:60,:,:,:),1),2)))
%     
%             curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(:,7)));

    %% split according to criteria
    
    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix

    for indDim = 1:4
    
    for indAtt = 1:4 % for each attribute sort by winning and non-winning choice 
        D{indAtt} = NaN(2,32,60,91,19); % condition*trial*channel*time matrix
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indAtt));
        % extract winning and missing options for trials available in the EEG data
        curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(:,7)));
        for indOption = 1:2
            includedTrials = find(data.TrlInfo(:,8) == indDim & curTrialWinMiss==indOption);
            %includedTrials = find(curTrialWinMiss==indOption);
            numIncluded = numel(includedTrials);
            for indTrial = 1:numIncluded
                D{indAtt}(indOption, indTrial,:,:,:) = permute(dataByRep(:,:,:,includedTrials(indTrial)),[4,1,2,3]);
            end
        end
    end

    M=size(D{1},1); N=size(D{1},2); O=size(D{1},4); T=size(D{1},5);

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we run 4 separate decodings.
    
    num_permutations=100;
    DA=NaN(indAtt,num_permutations,M,M,T);
    for indAtt = 1:4
        for perm =1:num_permutations
            tic
                        
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min([numel(find(~isnan(D{indAtt}(1,:,1,1,1)))); numel(find(~isnan(D{indAtt}(2,:,1,1,1))))]);
            permutedD{indAtt} = NaN(2,subsetAmount,60,O,T);
            for indCond = 1:2
                trialsAvailable = find(~isnan(D{indAtt}(indCond,:,1,1,1)));
                trialsSelected = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                permutedD{indAtt}(indCond,:,:,:,:) = D{indAtt}(indCond,trialsSelected,:,:,:);
            end

            % concatenate within-trial repetitions into pseudo-trials
            pseudo_trialD{indAtt} = [];
            % randomize repetition selection
            repSelect = randperm(19);
            for indRep = 1:19
                pseudo_trialD{indAtt} = cat(2, pseudo_trialD{indAtt}, permutedD{indAtt}(:,:,:,:,repSelect(indRep)));
            end

            %% perform SVM classification
            
            availReps = size(pseudo_trialD{indAtt},2);
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(pseudo_trialD{indAtt}(condA,1:floor(.75*availReps),:,time_point)) ; squeeze(pseudo_trialD{indAtt}(condB,1:floor(.75*availReps),:,time_point))];
                        MEEG_testing_data=[squeeze(pseudo_trialD{indAtt}(condA,floor(.75*availReps)+1:end,:,time_point)) ; squeeze(pseudo_trialD{indAtt}(condB,floor(.75*availReps)+1:end,:,time_point))];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,size(MEEG_training_data,1)/2) 2*ones(1,size(MEEG_training_data,1)/2)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test=[ones(1,size(MEEG_testing_data,1)/2) 2*ones(1,size(MEEG_testing_data,1)/2)];%[1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indAtt,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
%figure; plot(squeeze(nanmean(DA(1,:,1,2,:),2)))

    end % target attribute loop
    
    % average across permutations
    DA_end{indDim} = squeeze(mean(DA,2));
    DA_std{indDim} = squeeze(nanstd(DA,[],2));
    
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1:4,1,2,:),2),3),1)));
    %figure; plot(squeeze(nanmean(nanmean(nanmean(DA_end(1,1,2,:),2),3),1)));

    %% save results

    save([pn.out, ID, '_DecodeWinOption_SSVEP_Peak.mat'], 'DA_end', 'DA_std');
    
    end % dimensionality
    
end % function end