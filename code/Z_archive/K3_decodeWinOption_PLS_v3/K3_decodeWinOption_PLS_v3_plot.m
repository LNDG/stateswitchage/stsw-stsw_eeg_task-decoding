% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};


pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/J_lassoRegression/';
%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target/B_data/B_lassoRegression/';
%pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/B_data/B_lassoRegression/';

time = -1.5:.02:9.5;

DA_merged = cell(1,2); probabilityCourses = cell(1,2);
for indAge = 1:2
    indCount = 1;
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_PLSdecodeWinOption_v3.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            continue;
        end
        load(curFile);
        %plot(time,smoothts(squeeze(nanmean(info.accuracy,1)),'b',30))

        DA_merged{indAge}(indCount,:,:,:) = info.accuracy;
        probabilityCourses{indAge}(indCount,:,:,:) = info.probabilityCourse;
        weights{indAge}(indCount,:,:) = info.EEGweights;
        % encode in-set decoding
        for indCond = 1:4
            DecodedAcc{indAge}(indCount,indCond,:) = squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==indCond,:,:),1),2));
            for indAtt = 1:4
                DecodedAcc_Probed{indAge}(indCount,indCond,:) = squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==indCond & ...
                    info.TrlInfo(:,6)==indAtt,indAtt,:),1),2))-...
                    squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==indCond & ...
                    info.TrlInfo(:,6)==indAtt,find(~ismember([1:4],indAtt)),:),1),2));
            end
        end
        indCount = indCount+1;
    end
end

%% plot probability time courses for all features

figure; cla;
for indState = 1:4 % this is the trials with the attended feature
    subplot(4,1,indState); hold on;
    for indM = 1:2 % this is the trained model
        tmpData = squeeze(nanmean(probabilityCourses{1}(:,indState,indM,:),1));
        if indM == 2 % probability should be highest when trained model is applied to left-out set
            plot(time,smoothts(tmpData','b',1), 'LineWidth', 2)
        else
            plot(time,smoothts(tmpData','b',1), 'LineStyle', ':', 'LineWidth', 2)
        end
    end
    xlim([-.5 9])
end

% separate by on- or off-diagonal elements
for indAge = 1:2
    OnDiagonal{indAge} = []; OffDiagonal{indAge} = [];
    for indState = 1:4 % this is the trials with the attended feature
        for indM = 1:2 % this is the trained model
            tmpData = squeeze(probabilityCourses{indAge}(:,indState,indM,:));
            if indM == 2 % probability should be highest when trained model is applied to left-out set
                OnDiagonal{indAge} = cat(3,OnDiagonal{indAge}, tmpData);
            else
                OffDiagonal{indAge} = cat(3,OffDiagonal{indAge}, tmpData);
            end
        end
    end
end

figure; 
%imagesc(squeeze(nanmean(OnDiagonal,3))-squeeze(nanmean(OffDiagonal,3)))
patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
for indP = 1:numel(patches.timeVec)-1
    p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                [-1 -1 [1 1]], patches.colorVec(indP,:));
    p.EdgeColor = 'none';
end; hold on;
x = squeeze(nanmean(OnDiagonal{1},3))-squeeze(nanmean(OffDiagonal{1},3));
mean = squeeze(nanmean(x,1));
standError = nanstd(x,1)./sqrt(size(x,1));
dimPlot{1} = shadedErrorBar(time,mean,standError, ...
        'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .25);

x = squeeze(nanmean(OnDiagonal{2},3))-squeeze(nanmean(OffDiagonal{2},3));
mean = squeeze(nanmean(x,1));
standError = nanstd(x,1)./sqrt(size(x,1));
dimPlot{2} = shadedErrorBar(time,mean,standError, ...
        'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .25);
% plot(time,squeeze(nanmean(squeeze(nanmean(OnDiagonal{1},3))-squeeze(nanmean(OffDiagonal{1},3)),1)), 'k')
% plot(time,squeeze(nanmean(squeeze(nanmean(OnDiagonal{2},3))-squeeze(nanmean(OffDiagonal{2},3)),1)), 'r')
xlim([-.5 9]); ylim([-.3 .3]*10^-5)
ylabel('Probability cued vs. uncued model')
xlabel('Time (s)')
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine], {'YA'; 'OA'});
legend('boxoff')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% normalized probability courses

figure; cla;
for indState = 1:4
    subplot(4,1,indState); hold on;
    for indM = 1:4
        tmpData = squeeze(nanmean(probabilityCourses{1}(:,indState,indM,:)-nanmean(nanmean(probabilityCourses{1}(:,:,:,:),3),2),1));
        if indM == indState
            plot(time,smoothts(tmpData','b',10), 'LineWidth', 2)
        else
            plot(time,smoothts(tmpData','b',10), 'LineStyle', ':', 'LineWidth', 2)
        end
    end
    xlim([-.5 9])
end

%% plot results

addpath('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/shadedErrorBar')

for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = smoothts(squeeze(nanmean(DA_merged{indAge}(:,1,1:2,:),3)),'b',5);
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = smoothts(squeeze(nanmean(DA_merged{indAge}(:,2,1:2,:),3)),'b',5);
    DA_merged_3{indAge} = []; DA_merged_3{indAge} = smoothts(squeeze(nanmean(DA_merged{indAge}(:,3,1:2,:),3)),'b',5);
    DA_merged_4{indAge} = []; DA_merged_4{indAge} = smoothts(squeeze(nanmean(DA_merged{indAge}(:,4,1:2,:),3)),'b',5);
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = smoothts(squeeze(nanmean(nanmean(DA_merged{indAge}(:,:,1:2,:),3),2)),'b',5);
end

ageConditions = {'Young Adults'; 'Old Adults'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
    
h = figure;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [1 1]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    for indCond = 1:4
        grandAverage = squeeze(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),1));
        curData = squeeze(eval(['DA_merged_',num2str(indCond), '{indAge}']));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([.3 .7]); xlim([-.5 9]);
    legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine, dimPlot{3}.mainLine, dimPlot{4}.mainLine],...
        {'Att 1'; 'Att 2'; 'Att 3'; 'Att 4';})
    title(ageConditions{indAge});
end

h = figure;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [1 1]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    indCond = 1;
    grandAverage = squeeze(nanmean(eval(['DA_merged_all{indAge}']),1));
    curData = squeeze(eval(['DA_merged_all{indAge}']));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    ylim([.4 .6]); xlim([-.5 9]);
end

%% plot weight topographies with FieldTrip

    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools	= [pn.root, 'B_analyses/S2_TFR_PeriResponse/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = 'maxmin';

    h = figure('units','normalized','position',[.1 .1 .4 .4]);
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    for indFeature = 1:4
        subplot(1,4,indFeature);
        plotData.powspctrm = squeeze(nanmedian(weights{1}(:,:,indFeature),1))';
        ft_topoplotER(cfg,plotData);
    end

%% plot applied accuracies for higher loads

figure; 
hold on;
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==1 & info.TrlInfo(:,6) == 3,3,:),1),2)))
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==1 & info.TrlInfo(:,6) == 3,[1,2,4],:),1),2)))

figure; 
hold on;
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==1,:,:),1),2)))
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==2,:,:),1),2)))
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==3,:,:),1),2)))
plot(time,squeeze(nanmean(nanmean(info.accuracyByTrial(info.TrlInfo(:,8)==4,:,:),1),2)))

figure;
hold on;
for indCond = 2:4
    plot(time, squeeze(nanmedian(DecodedAcc{1}(:,indCond,:),1)))
end

figure;
cla; hold on;
for indCond = 1%:4
    plot(time, squeeze(nanmean(DecodedAcc_Probed{1}(:,indCond,:),1)))
end

