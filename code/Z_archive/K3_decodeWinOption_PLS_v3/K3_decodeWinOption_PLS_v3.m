% v4: include antagonistic examples as '0' for each model

function K3_decodeWinOption_PLS_v3(indID)

    % inputs:   ID   | indicator for ID list below

    %% add paths

    if ismac
        pn.root     = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.pls   = [pn.root, 'T_tools/[MEG]PLS/']; addpath(genpath(pn.pls));
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/J_lassoRegression/'];
        pn.pls      = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};
    %ID = '1118'

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
     %% CSD transform
    
    TrlInfo = data.TrlInfo;
    
    csd_cfg = [];
    csd_cfg.elecfile = [pn.root, 'T_tools/fieldtrip/template/electrode/standard_1005.elc'];
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    
    % 25 Hz low-pass
    
    TrlInfo = data.TrlInfo;
    
    cfg = [];
    cfg.lpfilter      = 'yes';
    cfg.lpfreq        = [25];
    cfg.lpfiltord     = 6;
    cfg.lpfilttype    = 'but';
    [data] = ft_preprocessing(cfg, data);
    data.TrlInfo = TrlInfo;
    
    % downsample to 50 Hz
    
    cfg = [];
    cfg.resamplefs = 50;
    cfg.detrend = 'yes';
    cfg.demean = 'yes';
    [data] = ft_resampledata(cfg, data);
%     
%     smoothVector = 1:5:size(data.trial{1},2);
%     for indTrial = 1:numel(data.trial)
%         data.trial{indTrial} = data.trial{indTrial}(:,smoothVector);
%         data.timeResamp = data.time{1}(smoothVector);
%     end
    
%     numofAtts = 1;
%     D = NaN(4,16,60,size(data.time{1},2)); % condition*trial*channel*time matrix
%     for indAtt = 1:4
%         disp(num2str(indAtt));    
%         includedTrials = find(data.TrlInfo(:,8) == numofAtts & data.TrlInfo(:,6) == indAtt);
%         numIncluded = numel(includedTrials);
%         for indTrial = 1:numIncluded
%             trialSize = size(data.trial{includedTrials(indTrial)},2);
%             D(indAtt,indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
%         end
%     end
    
    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix
    
    D = NaN(4,2,128,60,size(data.trial{1},2)); % condition*trial*channel*time matrix
    for indAtt = 1:4 % for each attribute sort by winning and non-winning choice 
        % (64 largely orthogonal trials across features; i.e., max. 32 trials per category)
        disp(num2str(indAtt));
        % extract winning and missing options for trials available in the EEG data
        curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(:,7)));
        for indOption = 1:2
            includedTrials = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == 1 & curTrialWinMiss==indOption);
            %includedTrials = find(curTrialWinMiss==indOption);
            numIncluded = numel(includedTrials);
            for indTrial = 1:numIncluded
                trialSize = size(data.trial{includedTrials(indTrial)},2);
                D(indAtt, indOption, indTrial,:,1:trialSize) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    %% 3) Decoding

    % get minimum requirements
    for indAtt = 1:4
        for indOption = 1:2
            trialsAvailable{indAtt,indOption} = find(~isnan(D(indAtt,indOption,:,1,1)));
        end
    end
    Ntrials = cellfun(@numel,trialsAvailable);
    info.NleftIn = min(min(Ntrials))-1;
    %info.NleftOut = Ntrials-info.NleftIn;
    
    option = [];
    option.method = 3; % [1] | 2 | 3 | 4 | 5 | 6
    option.num_perm = 500; %( single non-negative integer )
    option.num_split = 0; %( single non-negative integer )
    option.num_boot = 500; % ( single non-negative integer )
    option.cormode = 0; % [0] | 2 | 4 | 6
    option.meancentering_type = 0;% [0] | 1 | 2 | 3
    option.boot_type = 'strat'; %['strat'] | 'nonstrat'
    
    Nfolds=30;
    
    timeToTrain = [find(data.time{1}>3.2 & data.time{1}<3.8)];
    probabilityCourse = NaN(Nfolds,4,2,16,size(data.time{1},2));
    accuracy = NaN(Nfolds,4,2,16,size(data.time{1},2));
    topo = []; topo_wmask = [];
    for perm =1:Nfolds
        weights = [];
        disp(['Permutation ', num2str(perm), ' of ', num2str(Nfolds)]);
       % designate in- and out- trials
       % note that these are true for all trained models!
        for indAtt = 1:4
            for indOption = 1:2
                randTrials = randperm(numel(trialsAvailable{indAtt,indOption}));
                trialsIn{indAtt,indOption} = randTrials(1:info.NleftIn);
                trialsOut{indAtt,indOption} = randTrials(info.NleftIn+1:end);
            end
        end
        
        %% build models using PLS

        % Use a 'behavioral' PLS for trial-by-trial coupling (BUT: binary here)
        
        for model=1:4 % each model should represent a mix of in-set and out-of -set
            datamat_lst = cell(1); lv_evt_list = [];
            indCount = 1;
            for indOption = 1:2
                for indTrial = 1:numel(trialsIn{model,indOption})
                    tmp_data = squeeze(D(model, indOption, trialsIn{model,indOption}(indTrial),:,timeToTrain));
                    datamat_lst{1}(indCount,:) = reshape(tmp_data, [], 1);
                    lv_evt_list(indCount) = indOption-1;
                    indCount = indCount+1;
                end
            end
            option.stacked_behavdata = lv_evt_list';
            result = pls_analysis(datamat_lst, numel(lv_evt_list), 1, option);
            lvdat = squeeze(reshape(result.boot_result.compare_u(:,1), 60,1, numel(timeToTrain)));
            lvdat_u = squeeze(reshape(result.u(:,1), 60,1, numel(timeToTrain)));
            mask = double(lvdat > 3 | lvdat < -3);
            mask(mask==0) = NaN;
            topo(perm, :, model) = squeeze(nanmean(lvdat_u,2));
            topo_wmask(perm, :, model) = squeeze(nanmean(squeeze(mask.*lvdat_u),2));
            % check whether topos have to be inverted
            %figure; plot(topo(perm, :, model)); hold on; plot(topo_wmask(perm, :, model))
            r = corrcoef(result.usc(:,1), result.vsc(:,1));
            r = r(1,2);
            temp_Rs(perm,model) = r;
            u1 = nanmean(result.vsc(lv_evt_list==1,1));
            u2 = nanmean(result.vsc(lv_evt_list==0,1));
            if u1<u2 & r>0 || u1>u2 & r<0  % smaller expression for target (while having positive association) --> flip
                topo(perm, :, model) = -1.*topo(perm, :, model);
                topo_wmask(perm, :, model) = -1.*topo_wmask(perm, :, model);
            end
        end

        %% apply on out-of-sample data

        for indAtt = 1:4
            for indOption = 1:2
                curSignal = D(indAtt, indOption, trialsOut{indAtt,indOption},:,:);
                for indTrial = 1:numel(trialsOut{indAtt,indOption})
                    trlSignal = squeeze(curSignal(1,1,indTrial,:,:));
                    for indTime2 = 1:size(trlSignal,2)
                        model = indAtt; % Note: here it would make sense to encode options for all features, as thre is a correct option for each feature; 
                        % prediction model: intercept + beta*sensor
                        % MATLAB-specific: 
                        curPred = topo(perm, :,model)*trlSignal(:,indTime2);
                        curPred = 1./(1 + exp(-1.*(curPred-0)));
                        probabilityCourse(perm,indAtt,indOption,indTrial,indTime2) = curPred;
                        if curPred > .5
                            curPred = 2;
                        elseif curPred < .5
                            curPred = 1;
                        else curPred = 0;
                        end
                        if curPred == indOption
                            accuracy(perm,indAtt,indOption,indTrial,indTime2) = 1;
                        else
                            accuracy(perm,indAtt,indOption,indTrial,indTime2) = 0;
                        end
                    end
                end
            end
        end
    end % permutation
    % save mean model weights across permutations
    info.EEGweights = squeeze(nanmean(topo,1));
    info.EEGweights_wmask = squeeze(nanmean(topo_wmask,1));
    
    %% prepare outputs
    
    % average across trials
    info.probabilityCourse = [];
    info.probabilityCourse = squeeze(nanmean(nanmean(probabilityCourse(:,:,:,1,:),4),1)); % state * model * time
    info.accuracy = [];
    info.accuracy = squeeze(nanmean(nanmean(accuracy,4),1)); % state * time

%	figure; plot(data.time{1},smoothts(squeeze(nanmean(info.accuracy,1)),'b',10))
%     figure; plot(data.time{1},squeeze(nanmean(info.accuracy(1:4,:,:),1)))
%     figure; plot(data.time{1},squeeze(nanmean(info.probabilityCourse(4,:,:),1)))
%     figure; imagesc(data.time{1}, data.time{1}(timeToTrain),squeeze(nanmean(nanmean(nanmean(accuracy,3),2),1)))
%     figure; imagesc(squeeze(nanmean(info.probabilityCourse,2)))
%    figure; imagesc(info.EEGweights)
    
    %% apply decoder on all unlabeled trials to decode attended attributes
    
    accuracy = []; attributeLikelihood = [];
    for indTrial = 1:size(data.trial,2)
        for indTime = 1:size(data.trial{indTrial},2)
            for indDecoder = 1:4
                % apply decoder avg. across permutations (already defines across trials and avg. across temporally-specific decoders)
                curPred = data.trial{indTrial}(:,indTime)'*squeeze(info.EEGweights(:,indDecoder));
                curPred = 1./(1 + exp(-1.*(curPred-0)));
                attributeLikelihood(indTrial,indDecoder,indTime) = curPred;
                % include accuracy for EACH feature
                curTrialWinMiss = squeeze(winOptions.winningOptions(indDecoder, idx_win, data.TrlInfo(indTrial,7)));
                % turn into categorical predictions
                if curPred > .5
                    curPred = 2;
                elseif curPred < .5
                    curPred = 1;
                else curPred = 0;
                end
                if curPred == curTrialWinMiss
                    accuracy(indTrial,indDecoder,indTime) = 1;
                else
                    accuracy(indTrial,indDecoder,indTime) = 0;
                end
            end
        end
    end
    
    info.accuracyByTrial = accuracy;
    info.probabilityByTrial = attributeLikelihood;
    info.TrlInfo = data.TrlInfo;
    info.WinOptions = squeeze(winOptions.winningOptions(:, idx_win, data.TrlInfo(:,7)));
    
%     figure; imagesc(data.time{1},[],squeeze(nanmean(accuracy,1)))
%     figure; plot(data.time{1},squeeze(nanmean(nanmean(accuracy(data.TrlInfo(:,8)==1,:,:),1),2)))
    
     
    %% save outputs

    save([pn.out, ID, '_PLSdecodeWinOption_v3.mat'], 'info')
    
    %% WIP plotting topographies
%     
%     load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')
% 
%     cfg = [];
%     cfg.layout = 'acticap-64ch-standard2.mat';
%     cfg.parameter = 'powspctrm';
%     cfg.comment = 'no';
%     cfg.colorbar = 'EastOutside';
% 
%     figure
%     plotData = [];
%     plotData.label = elec.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(info.EEGweights(:,1)); ft_topoplotER(cfg,plotData);
%     cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
% 

end % function end