#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/IB_decodeTarget_D1

subjectAmount=100
for i in $(seq 1 $subjectAmount); do
	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name IB_decTarget_${i}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 1"				>> job.slurm
	echo "#SBATCH --mem 8gb" 						>> job.slurm
	echo "#SBATCH --time 03:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/IB_decTarget_${i}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./IB_decodeTarget_D1_run.sh /opt/matlab/R2016b $i" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done