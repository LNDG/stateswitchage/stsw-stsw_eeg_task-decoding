#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/I6_decodeWinOption_D1_allSubs

	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name I6_decodeWinOption_D1_allSubs" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 4"				>> job.slurm
	echo "#SBATCH --mem 32gb" 						>> job.slurm
	echo "#SBATCH --time=24:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/I6_decodeWinOption_D1_allSubs.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./I6_decodeWinOption_D1_allSubs_run.sh /opt/matlab/R2016b" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm