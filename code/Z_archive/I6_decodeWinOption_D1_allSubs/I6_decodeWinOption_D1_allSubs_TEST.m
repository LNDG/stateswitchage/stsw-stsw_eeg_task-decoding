
function I6_decodeWinOption_D1_allSubs

    % inputs:   ID   | indicator for ID list below
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.data    = [pn.root, 'B_data/'];
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    load([pn.data, 'A_winOptions.mat'], 'winOptions')
    
    indDim = 1;
    timeWindows = -.5:0.02:7.5;
    for indAtt = 1:4
        C{indDim,indAtt} = NaN(2,8*numel(IDs),60,numel(timeWindows)); % condition*trial*channel*time matrix
    end
    trialCount = ones(4,2);
    for indID = 1:numel(IDs)
        ID = IDs{indID};
        % load EEG data
        load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
        
        % high-pass-filter data above 25 Hz
        cfg = [];
        cfg.lpfilter = 'yes';
        cfg.lpfreq = 35;
        cfg.lpfiltord = 6;
        cfg.lpfilttype = 'but';
        dataHP = ft_preprocessing(cfg,data);
        
        % downsample highpassed time series
        for indTrial = 1:numel(data.trial)
            [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
            data.trial{indTrial} = dataHP.trial{indTrial}(:,tmpPoint);
        end
        % assign to matrix
        
        idx_win = find(strcmp(winOptions.IDs, ID)); % get index of current ID for win option matrix
        numIncluded = [];
        for indAtt = 1:4
            disp(num2str(indAtt));
            curTrial = find(data.TrlInfo(:,6) == indAtt & data.TrlInfo(:,8) == indDim & data.TrlInfo(:,10) == 1);
            curTrialWinMiss = squeeze(winOptions.winningOptions(indAtt, idx_win, data.TrlInfo(curTrial ,7)));
            for indOption = 1:2
                includedTrials = curTrial(curTrialWinMiss==indOption);
                trls2Enter = trialCount(indAtt,indOption):trialCount(indAtt,indOption)+numel(includedTrials)-1;
                C{indDim,indAtt}(indOption,trls2Enter,:,:) = permute(cat(3,data.trial{includedTrials}),[3,1,2]);
                trialCount(indAtt,indOption) = trialCount(indAtt,indOption)+numel(includedTrials)-1;
            end
        end
    end

    %% univariate noise normalization

    %baseline_time_points=1:1000; % pre-cue interval baseline

    M=size(C{1},1);
    N=size(C{1},2);
    O=size(C{1},3);
    T=size(C{1},4);

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we basically run 4 separate decodings. Note that we
    % always use the Dim1 condition to create the training set however.
    
    num_permutations=30;
    DA=NaN(4,num_permutations,M,M,T);
    for indAtt = 1:4
        for perm =1:num_permutations
            tic

            % select random subset of min. available trials across conditions in random order
            subsetAmount = 140;%min(min(trialCount));
            for indD = unique([1, indAtt])
                permutedD{indD} = NaN(2,subsetAmount,60,T);
                for indCond = 1:2
                    trialsAvailable          = find(~isnan(C{1,indD}(indCond,:,1,1)));
                    trialsSelected           = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                    permutedD{indD}(indCond,:,:,:) = C{1,indD}(indCond,trialsSelected,:,:);
                end
            end

            %% bin data into L=N/K pseudo trials
            
            K=7;
            L=subsetAmount/K;
            for indD = unique([1, indAtt])
                pseudo_trialD{indD}=NaN(M,L,O,T);
                for step=1:L %average by steps
                    trial_selector=(1+(step-1)*K):(K+(step-1)*K); %select trials to be averaged
                    pseudo_trialD{indD}(:,step,:,:)= mean(permutedD{indD}(:,trial_selector,:,:),2); %assign pseudo trial to pseudo_trial_D
                end
            end

            %L = subsetAmount;

            %% perform SVM classification
            
            for condA=1:M %loop through all conditions
                for condB = condA+1:M  %loop through all conditions >condA+1
                    for time_point =1:T % all time points are independent
                        % L-1 pseudo trials go to testing set, the Lth to training set
                        MEEG_training_data=[squeeze(pseudo_trialD{indAtt}(condA,1:end-1,1:60,time_point)) ; squeeze(pseudo_trialD{indAtt}(condB,1:end-1,1:60,time_point))];
                        MEEG_testing_data=[squeeze(pseudo_trialD{indAtt}(condA,end,1:60,time_point))' ; squeeze(pseudo_trialD{indAtt}(condB,end,1:60,time_point))'];
                        % you have to define class labels for the trials you will train and test
                        labels_train=[ones(1,L-1) 2*ones(1,L-1)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                        labels_test= [1 2]; % you will test on one trial from condA, and one from cond B
                        % train SVM
                        model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                        % test SVM
                        [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                        DA(indAtt,perm,condA,condB,time_point)=accuracy(1);
                    end % time point
                end % cond A
            end %cond B
            toc
        end % permutation
    end % target dimensionality loop
    
    % average across permutations
    DA_end = squeeze(nanmean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));

    figure; plot(timeWindows,squeeze(nanmean(nanmean(nanmean(DA_end(1:4,:,:,:),3),2),1)))
    
    %% save results

    save([pn.out, 'DecodeWinOption_D1_allSubs.mat'], 'DA_end', 'DA_std');
    
end % function end