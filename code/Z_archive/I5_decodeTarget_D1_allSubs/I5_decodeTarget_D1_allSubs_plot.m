% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
pn.out  = [pn.root, 'B_data/B_DecodingResults/'];

load([pn.out, 'DecodeTarget_D1_allSubs.mat'], 'DA_end', 'DA_std');

%% plot results
    
time = -.5:0.02:7.5;

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');
    
h = figure('units','normalized','position',[.1 .1 .15 .15]);
cla;
    patches.timeVec = [-.25 0 1 3 6 7.5];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot(time, squeeze(nanmean(nanmean(DA_end,2),1)), 'k','LineWidth', 2)
    
    ylim([45 65])
    xlabel('Time (s) from cue onset'); ylabel('Decoding accuracy (%)')
    xlim([-.25 7.25])
set(findall(gcf,'-property','FontSize'),'FontSize',18)


pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'I6_decodeWinOption_allSubs';
% % saveas(h, [pn.plotFolder, figureName], 'fig');
% % saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
