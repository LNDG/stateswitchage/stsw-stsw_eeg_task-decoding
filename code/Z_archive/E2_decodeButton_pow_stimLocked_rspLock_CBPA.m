% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')
addpath('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/T_tools/fieldtrip/')
ft_defaults;

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';
pn.data = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeButton_pow_rspLocked.mat'];
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_press_end;
    end
end

%% perform statistical testing (CBPA) with FieldTrip: derivative data (unsmoothed)

DA_merged{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge},4),3));

% DA_merged{indAge} sub x cond x time

dataYA = [];
for indID = 1:size(DA_merged{indAge},1)
    for indCond = 1:4
        dataYA{indCond, indID}.data = permute(squeeze(DA_merged{indAge}(indID,indCond,:)),[2,1]);
        dataYA{indCond, indID}.dimord = 'chan_time';
        dataYA{indCond, indID}.time = -100*.01:.01:.01*10;
        dataYA{indCond, indID}.label{1} = 'decoding';
    end
    % add chance comparison condition
    dataYA{5, indID} = dataYA{1, indID};
    dataYA{5, indID}.data = repmat(50, size(dataYA{1, indID}.data,1), size(dataYA{1, indID}.data,2));
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT'; % ft_statfun_depsamplesregrT
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_1] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{5,:});
[stat_2] = ft_timelockstatistics(cfgStat, dataYA{2,:}, dataYA{5,:});
[stat_3] = ft_timelockstatistics(cfgStat, dataYA{3,:}, dataYA{5,:});
[stat_4] = ft_timelockstatistics(cfgStat, dataYA{4,:}, dataYA{5,:});

figure;
subplot(2,2,1); hold on; plot(stat_1.mask)
subplot(2,2,2); hold on; plot(stat_2.mask)
subplot(2,2,3); hold on; plot(stat_3.mask)
subplot(2,2,4); hold on; plot(stat_4.mask)

save([pn.data, 'E_stats_decodeButton_rspLock.mat'], 'stat_1', 'stat_2','stat_3','stat_4');

%% do the same for stim-locked decoding

DA_merged = cell(1,2);
for indAge = 1
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeButton_pow_stimLocked.mat'];
        load(curFile);
        DA_merged{indAge}(id,:,:,:,:) = DA_end(:,:,:,1:end-10);
    end
end

% perform statistical testing (CBPA) with FieldTrip: derivative data (unsmoothed)

DA_merged{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge},4),3));

% DA_merged{indAge} sub x cond x time

dataYA = [];
for indID = 1:size(DA_merged{indAge},1)
    for indCond = 1:4
        dataYA{indCond, indID}.data = permute(squeeze(DA_merged{indAge}(indID,indCond,:)),[2,1]);
        dataYA{indCond, indID}.dimord = 'chan_time';
        dataYA{indCond, indID}.time = 2:0.01:8;
        dataYA{indCond, indID}.label{1} = 'decoding';
    end
    % add chance comparison condition
    dataYA{5, indID} = dataYA{1, indID};
    dataYA{5, indID}.data = repmat(50, size(dataYA{1, indID}.data,1), size(dataYA{1, indID}.data,2));
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT'; % ft_statfun_depsamplesregrT
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_1] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{5,:});
[stat_2] = ft_timelockstatistics(cfgStat, dataYA{2,:}, dataYA{5,:});
[stat_3] = ft_timelockstatistics(cfgStat, dataYA{3,:}, dataYA{5,:});
[stat_4] = ft_timelockstatistics(cfgStat, dataYA{4,:}, dataYA{5,:});

figure;
subplot(2,2,1); hold on; plot(stat_1.mask)
subplot(2,2,2); hold on; plot(stat_2.mask)
subplot(2,2,3); hold on; plot(stat_3.mask)
subplot(2,2,4); hold on; plot(stat_4.mask)

save([pn.data, 'E_stats_decodeButton_stimLock.mat'], 'stat_1', 'stat_2','stat_3','stat_4');
