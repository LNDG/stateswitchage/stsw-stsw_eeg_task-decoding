% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 47 YAs

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 53 OAs
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
pn.data = [pn.root, 'B_data/B_DecodingResults/'];

DA_merged = cell(1,2);
for indAge = 1:2
    %indCount = 1;
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_DecodeTargetMultiState_IB2.mat'];
        if ~exist(curFile)
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:) = NaN;
            continue;
        end
        load(curFile, 'DA_end');
        DA_merged{indAge}(id,:,:) = DA_end;
    end
end

%% plot results

time = -.5:0.01:7.5;
%time(1) = [];

ageConditions = {'Young Adults'; 'Old Adults'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(10,'RdBu');
    
h = figure('units','normalized','position',[.1 .1 .15 .3]);
cla;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [-.25 0 1 3 6 7.5];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    for indReg = 1:10
        curData = squeeze(DA_merged{indAge}(:,indReg,:));
        curData = smoothts(curData,'b',20);
        %plot(time,nanmean(curData,1), 'Color', cBrew(indReg,:), 'LineWidth', 2)
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indReg} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indReg,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([23 31])
    title(ageConditions{indAge});
    xlabel('Time (s) from cue onset'); ylabel('Decoding accuracy (%)')
    xlim([-.25 7.25])
    line([-1 7.25],[25 25], 'Color', 'k', 'LineWidth', 2, 'LineStyle', ':')
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
