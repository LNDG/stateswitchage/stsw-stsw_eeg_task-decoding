
function A4_decodeTargetOneVsAll_lasso(indID)

    % inputs:   ID   | indicator for ID list below

    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target/';
        pn.EEG      = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target/';
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_lassoRegression/'];
    end

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % downsample to 100 Hz: spatial PCA across 5 adjacent samples
    % this effectively removes the mean from each volume, save first two components
%     smoothVector = 1:5:size(data.trial{1},2);
%     for indTrial = 1:numel(data.trial)
%         disp(['Smoothing trial ... ' num2str(indTrial)])
%         for indSmooth = 1:numel(smoothVector)-1
%             [~, score] = pca(data.trial{indTrial}(:,smoothVector(indSmooth):smoothVector(indSmooth+1)), 'VariableWeights', 'variance', 'centered', true); 
%             data.trialResamp{indTrial}(:,indSmooth) = score(:,1);
%             data.trialResamp2{indTrial}(:,indSmooth) = score(:,2);
%         end
%     end
    
    smoothVector = 1:5:size(data.trial{1},2);
    for indTrial = 1:numel(data.trial)
        for indSmooth = 1:numel(smoothVector)-1
            data.trialMean{indTrial}(:,indSmooth) = squeeze(nanmedian(data.trial{indTrial}(:,smoothVector(indSmooth):smoothVector(indSmooth+1)),2));
        end
    end
    % align time vector
    for indSmooth = 1:numel(smoothVector)-1
        data.timeResamp(indSmooth) = nanmean(data.time{1}(smoothVector(indSmooth):smoothVector(indSmooth+1)));
    end
    
    numofAtts = 1;
    C = NaN(4,16,60,size(data.timeResamp,2)); % condition*trial*channel*time matrix
    for indAtt = 1:4
        disp(num2str(indAtt));    
        includedTrials = find(data.TrlInfo(:,8) == numofAtts & data.TrlInfo(:,6) == indAtt);
        numIncluded = numel(includedTrials);
        for indTrial = 1:numIncluded
            trialSize = size(data.trialMean{includedTrials(indTrial)},2);
            C(indAtt,indTrial,:,1:trialSize) = data.trialMean{includedTrials(indTrial)};
        end
    end

    %% univariate noise normalization

%     baseline_time_points=find(data.timeResamp>2.25 & data.timeResamp<2.75); % pre-trial interval baseline
% 
%     M=size(C,1); N=size(C,2); O=size(C,3); T=size(C,4);
% 
%     baseline_mean=nanmean(C(:,:,:,baseline_time_points),4);
%     baseline_std=nanstd(C(:,:,:,baseline_time_points),[],4);
%     D=(C-baseline_mean)./baseline_std; clear C;
    
    D = C; clear C;

    %% 3) Decoding

    % get minimum requirements
    for indAtt = 1:4
        trialsAvailable{indAtt} = find(~isnan(D(indAtt,:,1,1)));
    end
    Ntrials = cellfun(@numel,trialsAvailable);
    info.NleftIn = min(Ntrials-1);
    info.NleftOut = Ntrials-info.NleftIn;
    
    Nfolds=20;

    probabilityCourse = NaN(20,4,16,4,1100);
    accuracy = NaN(20,4,16,1100);
    for perm =1:Nfolds
        weights = [];
        disp(['Permutation ', num2str(perm), ' of ', num2str(Nfolds)]);
       % designate in- and out- trials
       % note that these are true for all trained models!
        for indAtt = 1:4
            randTrials = randperm(numel(trialsAvailable{indAtt}));
            trialsIn{indAtt} = randTrials(1:info.NleftIn);
            trialsOut{indAtt} = randTrials(info.NleftIn+1:end);
        end
        %timeToTrain = find(data.timeResamp>4.2 & data.timeResamp<4.4);
        timeToTrain = [find(data.timeResamp>3.2 & data.timeResamp<3.4),...
            find(data.timeResamp>4.2 & data.timeResamp<4.4), ...
            find(data.timeResamp>5.2 & data.timeResamp<5.4), ...
            find(data.timeResamp>6.2 & data.timeResamp<6.4), ...
            find(data.timeResamp>0.2 & data.timeResamp<0.4)];
        %for indTime =1:numel(timeToTrain)
        indTime = 1;
            %time_point = timeToTrain(indTime);
            for model=1:4
                % create leave- in and leave-out set plus labels
                MEEG_training_data = []; MEEG_test_data = []; LabelsIn = []; LabelsOut = [];
                for indAtt = 1:4
                    % add training data
                    
                    % wip: calculate spatial pca across time points for the
                    % calculation
                    
                    for itrial = 1:size(trialsIn{indAtt},2)
                        [~, score] = pca(squeeze(D(indAtt, trialsIn{indAtt}(itrial),:,timeToTrain)), 'VariableWeights', 'variance', 'centered', true);
                        tmp_data(itrial,:,1) = score(:,1);
                    end
                    tmp_corr = corrcoef(squeeze(tmp_data)');
                    tmp_idx = find(tmp_corr(:,1)<0);
                    tmp_data(tmp_idx,:) = tmp_data(tmp_idx,:)*-1; % invert trials where patterns are inverted
                    %tmp_data = squeeze(D(indAtt, trialsIn{indAtt},:,time_point));
                    MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
                    LabelsIn = cat(1,LabelsIn, repmat(indAtt==model,size(tmp_data,1),1)); clear tmp_data;
                    % add 'null' data to training
%                     tmp_data = squeeze(D(indAtt, trialsIn{indAtt},:,find(data.timeResamp>-.2,1,'first')));
%                     MEEG_training_data = cat(1, MEEG_training_data, tmp_data);
%                     LabelsIn = cat(1,LabelsIn, repmat(0,size(tmp_data,1),1)); clear tmp_data;
                    % add test data
                    for itrial = 1:size(trialsOut{indAtt},2)
                        [~, score] = pca(squeeze(D(indAtt, trialsOut{indAtt}(itrial),:,timeToTrain)), 'VariableWeights', 'variance', 'centered', true);
                        tmp_data(itrial,:,1) = score(:,1);
                    end
                    tmp_corr = corrcoef(squeeze(tmp_data)');
                    tmp_idx = find(tmp_corr(:,1)<0);
                    tmp_data(tmp_idx,:) = tmp_data(tmp_idx,:)*-1; % invert trials where patterns are inverted
                    %tmp_data = squeeze(D(indAtt, trialsOut{indAtt},:,time_point));
                    if size(tmp_data,2) ~= 60
                        tmp_data = tmp_data';
                    end
                    MEEG_test_data = cat(1, MEEG_test_data, tmp_data);
                    LabelsOut = cat(1,LabelsOut, repmat(indAtt==model,size(tmp_data,1),1)); clear tmp_data;
                end
                Lp = 7*10^-3;
                [weights(:,model,indTime), statsInfo] = lassoglm(MEEG_training_data, LabelsIn,'binomial','Alpha', 1, 'Lambda', Lp);
            %end
        end % end of model training

        % test on left-out samples
        % get probability of model 1:4
        
        for indAtt = 1:4
            curSignal = squeeze(D(indAtt, trialsOut{indAtt},:,:));
            for indTrial = 1:numel(trialsOut{indAtt})
                if numel(trialsOut{indAtt}) == 1
                    trlSignal = squeeze(curSignal(:,:));
                else
                    trlSignal = squeeze(curSignal(indTrial,:,:));
                end
                for indTime = 1:size(trlSignal,2)
                    for model = 1:4
                        curPred = trlSignal(:,indTime)'*squeeze(nanmean(weights(:,model,:),3));
                        % transform with sigmoid to get values between 0 and 1
                        probabilityCourse(perm,indAtt,indTrial,model,indTime) = 1./(1 + exp(-1.*(curPred-0)));
                    end
                  % calculate accuracy
                  [~, tmp_discreteCourse] = max(probabilityCourse(perm,indAtt,indTrial,:,indTime),[],4);
                  if tmp_discreteCourse == indAtt
                    accuracy(perm,indAtt,indTrial,indTime) = 1;
                  else
                    accuracy(perm,indAtt,indTrial,indTime) = 0;
                  end
                end
            end
        end
        % save mean model weights
        info.EEGweights(perm,:,:) = squeeze(nanmean(weights,3));
    end % permutation

    %% prepare outputs

    info.probabilityCourse = [];
    info.probabilityCourse = squeeze(nanmean(nanmean(probabilityCourse,3),1)); % state * model * time
    info.accuracy = [];
    info.accuracy = squeeze(nanmean(nanmean(accuracy,3),1)); % state * time

%     figure; imagesc(info.accuracy)
%     figure; plot(data.timeResamp,smoothts(squeeze(nanmean(info.accuracy,1)),'b',30))
%     figure; plot(data.timeResamp,squeeze(nanmean(info.accuracy,1)))
%     
%     figure; imagesc(squeeze(nanmean(info.probabilityCourse,2)))
% figure; plot(squeeze(nanmean(info.probabilityCourse,2))')
    
    %% save outputs

    save([pn.out, ID, '_logRegressionDecode_v1.mat'], 'info')

    %% figure section for debugging       

        %figure; imagesc(squeeze(weights(:,1,:)))

%     figure; hold on; 
%     plot(squeeze(nanmean(weights(:,1,:),3)))
%     plot(squeeze(nanmean(weights(:,2,:),3)))
%     plot(squeeze(nanmean(weights(:,3,:),3)))
%     plot(squeeze(nanmean(weights(:,4,:),3)))

    
%     figure; 
%     subplot(2,2,1); imagesc(data.timeResamp,[],squeeze(nanmean(nanmean(probabilityCourse(:,1,:,:,:),3),1)))
%     subplot(2,2,2); imagesc(data.timeResamp,[],squeeze(nanmean(nanmean(probabilityCourse(:,2,:,:,:),3),1)))
%     subplot(2,2,3); imagesc(data.timeResamp,[],squeeze(nanmean(nanmean(probabilityCourse(:,3,:,:,:),3),1)))
%     subplot(2,2,4); imagesc(data.timeResamp,[],squeeze(nanmean(nanmean(probabilityCourse(:,4,:,:,:),3),1)))
% 
%     figure; cla;
%     for indState = 1:4
%         subplot(4,1,indState); hold on;
%         for indM = 1:4
%             tmpData = squeeze(nanmean(nanmean(probabilityCourse(:,indState,:,indM,:),3),1));
%             if indM == indState
%                 plot(data.timeResamp,smoothts(tmpData','b',30), 'LineWidth', 2)
%             else
%                 plot(data.timeResamp,smoothts(tmpData','b',30), 'LineStyle', ':', 'LineWidth', 2)
%             end
%         end
%     end
% 
% 
%     figure; 
%     plot(data.timeResamp,smoothts(squeeze(nanmean(nanmean(nanmean(accuracy,3),1),2))','b',30))

    %% plot weight topographies with FieldTrip

%         pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
%         pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
%         addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
% 
%         load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')
% 
%         cfg = [];
%         cfg.layout = 'acticap-64ch-standard2.mat';
%         cfg.parameter = 'powspctrm';
%         cfg.comment = 'no';
%         cfg.colorbar = 'SouthOutside';
%         cfg.zlim = 'maxmin';
% 
%         h = figure('units','normalized','position',[.1 .1 .4 .4]);
%         plotData = [];
%         plotData.label = elec.label; % {1 x N}
%         plotData.dimord = 'chan';
%         plotData.powspctrm = squeeze(nanmean(info.EEGweights(:,:,1),1))';
%         ft_topoplotER(cfg,plotData);

end % function end