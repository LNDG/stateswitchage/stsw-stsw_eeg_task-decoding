#!/bin/bash

# call the BOSC analysis by session and participant
RootPath="/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/"

cd ${RootPath}/A_scripts/J1_decodeTargetOneVsAll_lasso_v4

subjectAmount=100
for i in $(seq 1 $subjectAmount); do
	echo '#!/bin/bash'                    			> job.slurm
	echo "#SBATCH --job-name STSWD_lasso_${i}" 	>> job.slurm
	echo "#SBATCH --cpus-per-task 2"				>> job.slurm
	echo "#SBATCH --mem 4gb" 						>> job.slurm
	echo "#SBATCH --time 08:00:00" 					>> job.slurm
	echo "#SBATCH --output ${RootPath}/Y_logs/J1_lasso_${i}.out"			>> job.slurm
	echo "#SBATCH --workdir ." 										>> job.slurm
	echo "./J1_decodeTargetOneVsAll_lasso_v4_run.sh /opt/matlab/R2016b $i" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done