% plot group decoding results: attribute, trained on Dim1 data, tested on
% Dim x data

clear all; clc;

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

% N = 42 YAs
IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 51 OAs (2131, 2237 removed due to missing run)
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/B_data/B_DecodingResults/';

DA_merged = cell(1,2);
for indAge = 1:2
    %indCount = 1;
    for id = 1:numel(IDs{indAge})
        curFile = [pn.data, IDs{indAge}{id}, '_I4C_decodeWinOption.mat'];
        try
            load(curFile);
            DA_merged{indAge}(id,:,:,:,:,:) = DA_end;
        catch
            disp(['File missing: ', IDs{indAge}{id}])
            DA_merged{indAge}(id,:,:,:,:,:) = NaN;
            continue;
        end
    end
end

%% plot results
    
time = -.5:0.05:7.5;
%time(1) = [];

for indAge = 1:2
    DA_merged_1{indAge} = []; DA_merged_1{indAge} = squeeze(nanmean(DA_merged{indAge}(:,1,:,:,:,:),3));
    DA_merged_2{indAge} = []; DA_merged_2{indAge} = squeeze(nanmean(DA_merged{indAge}(:,2,:,:,:,:),3));
    DA_merged_all{indAge} = []; DA_merged_all{indAge} = squeeze(nanmean(nanmean(DA_merged{indAge}(:,1:2,:,:,:,:),3),2));
end

ageConditions = {'Young Adults'; 'Old Adults'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

h = figure('units','normalized','position',[.1 .1 .15 .5]);
cla;
for indAge = 1:2
    subplot(2,1,indAge);
    patches.timeVec = [1 3 6 7.5];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [0 0 [100 100]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    plot([0,21], [50,50], 'k--', 'LineWidth',2)
    for indCond = 1:2
        grandAverage = squeeze(nanmean(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3),1));
        curData = smoothts(squeeze(nanmean(nanmean(eval(['DA_merged_',num2str(indCond), '{indAge}']),2),3)),'b',5);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
            'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
    ylim([45 56])
    title(ageConditions{indAge});
    xlabel('Time (s)'); ylabel('Decoding accuracy (%)')
    xlim([0 7.25])
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)
legend([dimPlot{1}.mainLine, dimPlot{2}.mainLine],...
        {'Probed'; 'Uncued'}, 'orientation', 'vertical', 'location', 'NorthWest')
legend('boxoff')
suptitle({'VC decoding';'prevalent option'})

