% Get task design information for EEG session

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

%% load behavioral data

behavData = load(fullfile(rootpath, '..', '..', 'stsw_beh_task', 'behavior', ...
    'data', 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'));

%% extract data by subject

for indID = 1:numel(IDs)
        
    %% get behavioral data (incl. condition assignment)

    behavIdx = find(strcmp(behavData.IDs_all, IDs{indID})); % get index of current subject in behavioral matrix
    SubjBehavData.Atts          = behavData.MergedDataEEG.Atts(:,behavIdx);
    SubjBehavData.StateOrders   = behavData.MergedDataEEG.StateOrders(:,behavIdx);
    SubjBehavData.RTs           = behavData.MergedDataEEG.RTs(:,behavIdx);
    SubjBehavData.Accs          = behavData.MergedDataEEG.Accs(:,behavIdx);
    SubjBehavData.CuedAttributes = reshape(cat(1,behavData.MergedDataEEG.expInfo{behavIdx}.AttCuesRun{:})',256,1);

    % create matrix with crucial condition info
    TrlInfo = NaN(256,7);
    for indTrial = 1:256
       tmp_cuedTrial = SubjBehavData.CuedAttributes(indTrial);
       if ismember(1,tmp_cuedTrial{:})
           TrlInfo(indTrial,1) = 1;
       end
       if ismember(2,tmp_cuedTrial{:})
           TrlInfo(indTrial,2) = 1;
       end
       if ismember(3,tmp_cuedTrial{:})
           TrlInfo(indTrial,3) = 1;
       end
       if ismember(4,tmp_cuedTrial{:})
           TrlInfo(indTrial,4) = 1;
       end
    end
    TrlInfo(:,5) = repmat(1:8,1,32)'; % serial position
    TrlInfo(:,6) = SubjBehavData.Atts;
    TrlInfo(:,7) = 1:256;
    TrlInfo(:,8) = SubjBehavData.StateOrders;
    TrlInfo(:,9) = SubjBehavData.RTs;
    TrlInfo(:,10) = SubjBehavData.Accs;
    
    %% add winning options for each feature
    
    % get trials where two options (in two-state scenario) align
    % operationalized via the relative response agreement among the cues

    winningOptions = NaN(256,4);
    count = 1;
    for indRun = 1:4 % loop across runs
        for indBlock = 1:8 % loop across blocks
            for indTrial = 1:8 % loop across trials
                if ~isempty(behavData.MergedDataEEG.expInfo{behavIdx})
                    cuedAttributes = behavData.MergedDataEEG.expInfo{behavIdx}.AttCuesRun{indRun}{indBlock,indTrial};
                    for indFeature = 1:4
                        winningOptions(count,indFeature) = behavData.MergedDataEEG.expInfo{behavIdx}.HighProbChoiceRun{indRun}{indBlock,indTrial}(indFeature);
                    end
                    count = count + 1; % increase trial number;
                else
                   winningOptions(:,:) = NaN;
                end
            end
        end
    end
    
    TrlInfo(:,11:14) = winningOptions;
    
    %% add combinatorials
    
    addpath(fullfile(rootpath, '..', '..', 'stsw_beh_task', 'behavior', 'tools', 'allcomb'));
    
    % get all 16 unique sequences
    combs = allcomb([1,2],[1,2],[1,2],[1,2]);
    % get indication for the winning options for all trials still in the EEG recording
    curTrlWins = TrlInfo(:,11:14);
    % for each trial, assign combination ID
    for indTrial = 1:size(curTrlWins,1)
        TrlInfo(indTrial,15) = double(find(ismember(combs, curTrlWins(indTrial,:), 'rows')));        
    end
    
    %% add info
    
    TrlInfoLabels = {'Att1 cued', 'Att2 cued', 'Att3 cued', 'Att4 cued', ...
        'Serial position', 'WinningAtt', 'Continuous position', 'Cue Dimensionality',...
        'RT', 'Acc', 'Feat1Win', 'Feat2Win', 'Feat3Win', 'Feat4Win', 'Combinatorial'};
    
    %% remove all trials that were excluded during preprocessing
    
    trlinfos_eeg = [];
    for indRun = 1:4
        load(fullfile(rootpath, '..', '0_preproc','data','D_History',...
            [IDs{indID},'_r',num2str(indRun),'_dynamic_config.mat']));
        trlinfos_eeg = cat(1, trlinfos_eeg, config.trl);
    end
    
    TrlInfo = TrlInfo(trlinfos_eeg(:,4)==1,:);
    
    %% save TrialInfo
    
    pn.dataOut = fullfile(rootpath, 'data', 'trialInfo');
    save(fullfile(pn.dataOut,[IDs{indID},'_TrlInfo_mat']),'TrlInfo', 'TrlInfoLabels')
    
end
