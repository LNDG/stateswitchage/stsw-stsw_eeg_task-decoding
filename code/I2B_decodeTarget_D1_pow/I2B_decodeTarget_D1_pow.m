
% This version employs two differences: 1. use 4-way classifier, 2. do not
% z-score individual indices for solution

function I2B_decodeTarget_D1_pow(indID)

    % inputs:   ID   | indicator for ID list below
    
    %% add paths

    if ismac
        pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S12_SVMdecode_target_v2/';
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/']; 
        pn.tools    = [pn.root, 'T_tools/']; addpath(pn.tools);
        pn.EEG      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/X1_preprocEEGData/';
        addpath(genpath(pn.libsvm));
        pn.FieldTrip    = [pn.root, 'T_tools/fieldtrip/']; addpath(pn.FieldTrip); ft_defaults;
    else
        pn.root     = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S12_SVMdecode_target_v2/';
        pn.EEG      = '/home/mpib/LNDG/StateSwitch/WIP_eeg/X1_preprocEEGData/';
        pn.out      = [pn.root, 'B_data/B_DecodingResults/'];
        pn.libsvm   = [pn.root, 'T_tools/libsvm-3.11/'];
    end

    mex -setup
    cd([pn.libsvm,'matlab/']);
    make

    % N = 47 YAs + 53 OAs;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};

    ID = IDs{str2num(indID)};
%   ID = '1118';

    % load EEG data
    load([pn.EEG,ID,'_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'])
    
    % perform a TFR, get frequency bins, downsample
    
    cfg = [];
    cfg.method     = 'wavelet';
    cfg.width      = 5;
    cfg.output     = 'pow';
    cfg.foi        = 30;
    cfg.toi        = -.5:0.01:7.5;
    cfg.keeptrials = 'yes';
    TFRwave = ft_freqanalysis(cfg, data);
    
     % multitaper for gamma
    
%     cfg = [];
%     cfg.method = 'mtmconvol';
%     cfg.keeptapers  = 'no';
%     cfg.taper = 'dpss'; % high frequency-optimized analysis (smooth)
%     cfg.foi = 40:20:140;
%     cfg.tapsmofrq = ones(length(cfg.foi),1) .* 8;
%     cfg.t_ftimwin = ones(length(cfg.foi),1) .* 0.4;
%     cfg.toi = -.5:0.01:7.5;
%     cfg.keeptrials = 'yes';
%     cfg.pad = 'nextpow2';
%     Gamma = ft_freqanalysis(cfg, data);
%     
%     % gamma normalization
%         
%     allData = log10(Gamma.powspctrm);
%     idxTime = Gamma.time > 2.2 & Gamma.time<2.9;
%     meanData= repmat(nanmean(nanmean(allData(:,:,:,idxTime),4),1),size(allData,1),1,1,numel(Gamma.time));
%     stdData= repmat(nanstd(nanmean(allData(:,:,:,idxTime),4),[],1),size(allData,1),1,1,numel(Gamma.time));
%     allData = (allData-meanData)./stdData; % remove grand mean, normalize by standard deviation across all trials
%     Gamma.powspctrm = allData;
    
    % average in freq ranges of interest
    
%     freqRanges = [8,25];
%     for indFreq = 1:numel(freqRanges)-1
%         curFreq = TFRwave.freq >= freqRanges(indFreq) & TFRwave.freq<freqRanges(indFreq+1);
%         TFRwave.powavg(:,:,indFreq,:) = squeeze(nanmean(TFRwave.powspctrm(:,:,curFreq,:),3));
%     end

%     freqRanges = [40,140];
%     for indFreq = 1:numel(freqRanges)-1
%         curFreq = Gamma.freq >= freqRanges(indFreq) & Gamma.freq<freqRanges(indFreq+1);
%         Gamma.powavg(:,:,indFreq,:) = squeeze(nanmean(Gamma.powspctrm(:,:,curFreq,:),3));
%     end
    
    % downsample broadband time series
    timeWindows = -.5:0.01:7.5;    
    for indTrial = 1:numel(data.trial)
        [~, tmpPoint] = min(abs(timeWindows-data.time{1}'));
        data.trial{indTrial} = data.trial{indTrial}(:,tmpPoint);
    end

    % concatenate power values for decoding (no zscore here!)
    for indTrial = 1:numel(data.trial)
        data.trial{indTrial} = cat(1, data.trial{indTrial}, ...
            squeeze(TFRwave.powspctrm(indTrial,44:60,1,:)));
    end
%     for indTrial = 1:numel(data.trial)
%         data.trial{indTrial} = cat(1, zscore(data.trial{indTrial},[],1), ...
%             zscore(squeeze(TFRwave.powavg(indTrial,:,1,:)),[],1), ...
%             zscore(squeeze(Gamma.powavg(indTrial,:,1,:)),[],1));
%     end
    
    O = size(data.trial{1},1);
    for indDim = 1:4
        C{indDim} = NaN(4,16,O,numel(timeWindows)); % condition*trial*channel*time matrix
        for indAtt = 1:4
            disp(num2str(indAtt));    
            includedTrials = find(data.TrlInfo(:,8) == indDim & data.TrlInfo(:,6) == indAtt);
            numIncluded(indDim, indAtt) = numel(includedTrials);
            for indTrial = 1:numIncluded(indDim, indAtt)
                C{indDim}(indAtt,indTrial,:,:) = data.trial{includedTrials(indTrial)};
            end
        end
    end

    %% Sanity-check: plot topographies
    
%     pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
%     pn.tools	= [pn.root, 'B_analyses/S2_TFR_PeriResponse/T_tools/']; addpath(pn.tools);
%     addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
% 
%     load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')
% 
%     cfg = [];
%     cfg.layout = 'acticap-64ch-standard2.mat';
%     cfg.parameter = 'powspctrm';
%     cfg.comment = 'no';
%     cfg.colorbar = 'SouthOutside';
%     cfg.zlim = 'maxmin';
% 
%     h = figure('units','normalized','position',[.1 .1 .2 .2]);
%     plotData = [];
%     plotData.label = elec.label; % {1 x N}
%     plotData.dimord = 'chan';
%     chanInfo = squeeze(nanmean(nanmean(C{1}(4,:,:,timeWindows>3 & timeWindows<6),4),2));
%     plotData.powspctrm = chanInfo(61:120); % 1:60 : broadband, 61:120 : alphabeta; 121:180 : gamma
%     ft_topoplotER(cfg,plotData);
%     colormap('parula')
    
%     figure; imagesc(timeWindows,[],squeeze(nanmean(D{1}(1,:,:,:),2)-nanmean(D{1}(4,:,:,:),2)))
    
    %% univariate noise normalization

    baseline_time_points=timeWindows>2 & timeWindows<2.9; % pre-cue interval baseline

    M=size(C{1},1);
    N=size(C{1},2);
    O=size(C{1},3);
    T=size(C{1},4);

    for indDim = 1:4
        baseline_mean=nanmean(C{indDim}(:,:,:,baseline_time_points),4);
        baseline_std=nanstd(C{indDim}(:,:,:,baseline_time_points),[],4);
        D{indDim}=(C{indDim}-baseline_mean)./baseline_std;
        %D{indDim}=C{indDim};
    end; clear C;

    %% 3) Decoding

    % Here, we set up a new decoding framework for each dimensionality
    % condition, i.e. we basically run 4 separate decodings. Note that we
    % always use the Dim1 condition to create the training set however.
    
    num_permutations=100;
    DA=NaN(4,num_permutations,T);
    for indDim = 1:4
        for perm =1:num_permutations
            tic
            
            % select random subset of min. available trials across conditions in random order
            subsetAmount = min(min(numIncluded));
            for indD = unique([1, indDim])
                permutedD{indD} = NaN(4,subsetAmount,O,T);
                for indCond = 1:4
                    trialsAvailable          = find(~isnan(D{indD}(indCond,:,1,1)));
                    trialsSelected           = trialsAvailable(randperm(numel(trialsAvailable),subsetAmount));
                    permutedD{indD}(indCond,:,:,:)      = D{indD}(indCond,trialsSelected,:,:);
                end
            end

            %% bin data into L=N/K pseudo trials
            % Not done here due to low N.

            L = subsetAmount;

            %% perform SVM classification
            
            for time_point =1:T % all time points are independent
                % L-1 pseudo trials go to testing set, the Lth to training set
                MEEG_training_data=[squeeze(permutedD{indDim}(1,1:end-1,:,time_point)) ; ...
                    squeeze(permutedD{indDim}(2,1:end-1,:,time_point)); ...
                    squeeze(permutedD{indDim}(3,1:end-1,:,time_point)); ...
                    squeeze(permutedD{indDim}(4,1:end-1,:,time_point))];
                MEEG_testing_data=[squeeze(permutedD{indDim}(1,end,:,time_point))' ; ...
                    squeeze(permutedD{indDim}(2,end,:,time_point))'; ...
                    squeeze(permutedD{indDim}(3,end,:,time_point))'; ...
                    squeeze(permutedD{indDim}(4,end,:,time_point))'];
                % you have to define class labels for the trials you will train and test
                labels_train=[ones(1,L-1) 2*ones(1,L-1) 3*ones(1,L-1) 4*ones(1,L-1)]; %they are L times 1, and L times 2 in a row (you just label any classes as 1 and 2, as the SVM at any time point only sees 2 classes
                labels_test= [1 2 3 4]; % you will test on one trial from condA, and one from cond B
                % train SVM
                model = svmtrain(labels_train', MEEG_training_data,'-s 0 -t 0 -q');
                % test SVM
                [predicted_label, accuracy, decision_values] = svmpredict(labels_test', MEEG_testing_data , model);
                DA(indDim,perm,time_point)=accuracy(1);
            end % time point
            toc
        end % permutation

    end % target dimensionality loop
    
    % average across permutations
    DA_end = squeeze(mean(DA,2));
    DA_std = squeeze(nanstd(DA,[],2));

    % figure; imagesc(timeWindows,[],DA_end)
    
    %% save results

    save([pn.out, ID, '_DecodeTargetMultiState_pow_I2B.mat'], 'DA_end', 'DA_std');
    
end % function end